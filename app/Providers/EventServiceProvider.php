<?php
declare(strict_types=1);

namespace App\Providers;

use App\Events\Articles\ArticleCreated;
use App\Events\Comments\CommentCreated;
use App\Events\Users\UserPasswordForgot;
use App\Events\Users\UserRegistered;
use App\Listeners\Articles\AddNewArticleSendMail;
use App\Listeners\Articles\ArticleSlugUpdate;
use App\Listeners\Comments\AddCommentToEntityTypeSendMail;
use App\Listeners\Users\NewUserPasswordSendMail;
use App\Listeners\Users\RegisteredUserSendMail;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        ArticleCreated::class => [
            ArticleSlugUpdate::class,
            AddNewArticleSendMail::class,
        ],
        CommentCreated::class => [
            AddCommentToEntityTypeSendMail::class
        ],
        UserRegistered::class => [
            RegisteredUserSendMail::class
        ],
        UserPasswordForgot::class => [
            NewUserPasswordSendMail::class
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
