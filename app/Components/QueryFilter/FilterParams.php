<?php
declare(strict_types=1);

namespace App\Components\QueryFilter;

/**
 * Class FilterParams
 * @package App\Components\QueryFilter
 */
class FilterParams implements FilterParamsContract
{
    /**
     * @var array
     */
    protected array $params;

    /**
     * FilterParams constructor.
     *
     * @param array $params
     */
    public function __construct(array $params)
    {
        $this->params = $params;
    }

    /**
     * @return array
     */
    public function filter(): array
    {
        return $this->params[self::TYPE_FILTER] ?? [];
    }

    /**
     * @return string
     */
    public function sort(): string
    {
        return $this->params[self::TYPE_SORT] ?? '';
    }

    /**
     * @param string $type
     * @param        $value
     */
    public function setParams(string $type, $value): void
    {
        $this->params[$type] = $value;
    }
}
