"use strict";

function toggleFilterMenu(elem) {
    let filterMenu = elem.parentNode.children[1];
    filterMenu.style.display = filterMenu.style.display === 'none' ? 'block' : 'none';
}