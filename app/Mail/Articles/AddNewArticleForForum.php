<?php
declare(strict_types=1);

namespace App\Mail\Articles;

use App\Models\Article;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Throwable;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AddNewArticleForForum extends Mailable
{
    use Queueable;
    use SerializesModels;

    public Article $article;
    public User $user;

    /**
     * Create a new message instance.
     *
     * @param Article $article
     * @param User $user
     */
    public function __construct(Article $article, User $user)
    {
        $this->article = $article;
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->from(\config('mail.from.address'))
            ->view('mail.articles.add-new-article-for-forum')
            ->subject('Add new article for forum');
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        Log::error('Error send mail add new article for forum', [$exception]);
    }
}
