export default function (obj, text = '') {
    obj.$notify({
        group: 'error',
        title: 'Error',
        speed: 1500,
        text: text ? text : 'Произошла ошибка'
    });
}

