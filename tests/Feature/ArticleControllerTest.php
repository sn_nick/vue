<?php

namespace Tests\Feature;

use App\Components\Repository\ArticleRepository;
use App\Http\Controllers\API\ArticleController;
use App\Http\Requests\Articles\IndexRequest;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ArticleControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex(): void
    {
        $request = IndexRequest::create(
            '/articles',
            'GET',
            []
        );

        $repository = new ArticleRepository();

        $controller = new ArticleController($repository);
        $resource = $controller->index($request);

        self::assertEquals(Response::HTTP_OK, $resource->response()->getStatusCode());
    }
}
