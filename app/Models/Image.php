<?php
declare(strict_types=1);

namespace App\Models;

use App\Traits\ScopeFilter;
use Illuminate\Database\Eloquent\{Builder, Collection, Model, SoftDeletes};
use Illuminate\Database\Eloquent\Relations\{BelongsTo, HasMany, MorphMany};
use Illuminate\Support\Facades\DB;
use Spatie\ModelStatus\HasStatuses;

class Image extends Model
{
    use ScopeFilter;
    use SoftDeletes;
    use HasStatuses;

    protected $fillable = [
        'name',
        'slug',
        'img',
        'old_link',
        'image_orientation_id',
        'description',
        'like',
        'view',
        'theme_id',
        'image_category_id',
        'site_id',
        'user_id',
    ];

    /**
     * @var int
     */
    protected $perPage = 40;

    protected $appends = ['image_src', 'synthetic'];

    public function site(): BelongsTo
    {
        return $this->belongsTo(Site::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function imageCategory(): BelongsTo
    {
        return $this->belongsTo(ImageCategory::class);
    }

    public function sort(): BelongsTo
    {
        return $this->belongsTo(Sort::class);
    }

    public function imageOrientation(): BelongsTo
    {
        return $this->belongsTo(ImageOrientation::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'entity_id', 'id')
                    ->where('entity_type_id', '=', EntityType::IMAGE_ID);
    }

    public function entity_comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'entity');
    }

    public function scopeTie(Builder $builder): Builder
    {
        return $builder->with('site', 'user.profile', 'imageCategory');
    }

    public function scopeSimilar(Builder $builder, string $slug): Builder
    {
        return $builder->inRandomOrder()
                       ->with('site', 'user.profile')
                       ->where('slug', '!=', $slug)
                       ->limit(6);
    }

    public function getImageSrcAttribute()
    {
        return $this->attributes['image_src'] = \get_dir_img($this);
    }

    public function getSyntheticAttribute(): array
    {
        $this->attributes['synthetic']['count_comments'] = $this->comments()->count();
        $this->attributes['synthetic']['image_src'] = \get_dir_img($this);

        return $this->attributes['synthetic'];
    }

    public function insertGetImages(array $rows): Collection
    {
        $latest = DB::table('images')->select('id')->orderBy('id', 'DESC')->first();

        DB::table('images')->insert($rows);

        return $this->where('id', '>', $latest->id)->get();
    }

    public function insertWithStatus(array $rows, $status): Collection
    {
        $images = $this->insertGetImages($rows);

        foreach($images as $key => $image) {
            $statuses[$key]['name'] = $status;
            $statuses[$key]['model_id'] = $image->id;
            $statuses[$key]['model_type'] = 'image';
            $statuses[$key]['created_at'] = now();
            $statuses[$key]['updated_at'] = now();
        }

        DB::table('statuses')->insert($statuses ?? []);

        return $images;
    }

    public function getStorageDirectoryFilePathForUpload(string $filename): string
    {
        return sprintf(
            storage_path('app/public/img/uploaded/%s/%s'),
            mb_substr($filename,0,1,"UTF-8"),
            mb_substr($filename,1,2,"UTF-8"),
            $filename
        );
    }
}
