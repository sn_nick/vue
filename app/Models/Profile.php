<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\ModelStatus\HasStatuses;

class Profile extends Model
{
    use HasStatuses;

    protected $fillable = [
        'user_id',
        'full_name',
        'sex_id',
        'date_birth',
        'description',
        'facebook',
        'instagram',
        'is_mail_blog_news',
        'is_mail_forum_news',
        'is_mail_answer_entity',
        'img',
    ];

    protected $casts = [
        'date_birth' => 'date',
    ];

    protected $appends = ['image_src'];

    public function getImageSrcAttribute(): string
    {
        return $this->attributes['image_src'] = !empty($this->img) ?
            $this->img :
            '/storage/img/profile/profile_image_dummy.svg';
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
