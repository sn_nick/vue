<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Новый комментарий (New comment)</title>
</head>
<body>
<h1>Respected, {{$entity->user->profile->full_name}}!</h1>
<p>A new comment has been sent to you by publication <b>{{$entity->name}}</b>!</p>


<h1>Уважаемый, {{$entity->user->profile->full_name}}!</h1>
<p>Вам отправлен новый комментарий к публикации: <b>{{$entity->name}}</b>!</p>
</body>
</html>
