<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
* @method static \App\Traits\ScopeFilter filter($filters)
 */
trait ScopeFilter
{
    public function scopeFilter(Builder $builder, $filters): Builder
    {
        return $filters->apply($builder);
    }
}
