<?php
declare(strict_types=1);

namespace App\Components\QueryFilter\Users;

use App\Components\QueryFilter\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class UsersIndexFilter extends QueryFilter
{
    public function default(): Builder
    {
        return $this->builder;
    }
}