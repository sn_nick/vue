<?php
declare(strict_types=1);

namespace App\Components\UploadingFile;

use App\Components\UploadingFile\DTO\ImageFileDTO;
use App\Models\Image;
use App\Models\Status;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\File\File;

final class ImageFile
{
    protected Image $model;
    protected ImageFileDTO $dto;
    protected ImageFileUploading $fileUploading;

    public function __construct(ImageFileDTO $dto)
    {
        $this->model = new Image();
        $this->dto = $dto;
        $this->fileUploading = new ImageFileUploading($this->model, $this->dto);
    }

    public function create(): Collection
    {
        $this
            ->dto
            ->getImageFiles()
            ->map(fn(UploadedFile $file): File => $this->fileUploading->upload($file))
            ->toArray();

        return $this->model->insertWithStatus($this->fileUploading->getData(), Status::ACCEPTED_STATUS);
    }
}
