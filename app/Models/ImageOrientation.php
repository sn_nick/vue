<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ImageOrientation extends Model
{
    public const HORIZONTAL_ID = 1;
    public const VERTICAL_ID = 2;

    protected $fillable = [
        'name',
        'type',
    ];

    public $timestamps = false;

    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }
}
