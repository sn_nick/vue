<?php
declare(strict_types=1);

namespace App\Http\Requests\Users;

use App\Traits\RequestParams;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends FormRequest
{
    use RequestParams;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return (bool)Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->setAllowedParams(['name',]);

        return [
            'name' => ['required', 'string'],
        ];
    }
}
