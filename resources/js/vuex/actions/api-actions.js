import axios from "axios";
import route from "../../halpers/route";
import constants from "../../halpers/constants";
import notify_errors from "../../halpers/notify_errors";
import notify_error from "../../halpers/notify_error";

export default {
    GET_IMAGES({commit}) {
        axios.get(route('images.index'))
            .then((response) => {

                commit('SET_IMAGES_CATALOG', response.data.data);
                //console.log(response.data);
                return response;
            })
            .catch((error) => {
                notify_error(this);
            });
    },
    GET_IMAGE({commit}, slug) {
            axios.get(route('images.show', [slug]))
            .then((response) => {

                commit('SET_IMAGE', response.data.data);
                commit('SET_IMAGES_SIMILAR', response.data.meta.images);

                return response;
            })
            .catch((error) => {
                notify_error(this);
            });
    },
    GET_ARTICLES_FORUM({commit}) {
        let filters = {article_type_id: constants.article_types.forum};
        let url_params = {
            page: 1,
            filters: filters,
        }
        axios.get(route('articles.index'), {
            params: url_params
        })
            .then((response) => {

                commit('SET_ARTICLES_FORUM', response.data.data);
                //console.log(response.data);
                return response;
            })
            .catch((error) => {
                notify_error(this);
            });
    },
    GET_ARTICLES_DASHBOARD({commit}) {
        let url_params = {
            page: 1,
            filters: {articles: {article_type_id: constants.article_types.forum}},
        }
        axios.get(route('profiles.show'), {
            params: url_params
        })
            .then((response) => {
                commit('SET_ARTICLES_DASHBOARD', response.data.data.articles);
                return response;
            })
            .catch((error) => {
                notify_error(this);
            });
    },
    GET_ARTICLE({commit}, slug) {
        axios.get(route('articles.show', [slug]))
            .then((response) => {

                commit('SET_ARTICLE', response.data.data);
                console.log(response.data.data);
                return response;
            })
            .catch((error) => {
                notify_error(this);
            });
    },
    GET_DICTIONARIES({commit}) {
        axios.get(route('dictionaries'))
            .then((response) => {

                commit('SET_DICTIONARIES', response.data.dictionaries);
                //console.log(response.data);
                return response;
            })
            .catch((error) => {
                notify_error(this);
            });
    },
}
