<?php
declare(strict_types=1);

namespace App\Http\Requests\Profiles;

use App\Traits\RequestParams;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    use RequestParams;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return (bool)Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->setAllowedParams([
                'full_name',
                'sex_id',
                'date_birth',
                'description',
                'facebook',
                'instagram',
                'is_mail_blog_news',
                'is_mail_forum_news',
                'is_mail_answer_entity',
                'img',
            ]
        );

        return [
            'full_name' => 'string',
            'sex_id' => 'numeric|in:0,1,2',
            'date_birth' => 'string|date',
            'description' => 'nullable|string',
            'facebook' => 'nullable|string|url',
            'instagram' => 'nullable|string|url',
            'is_mail_blog_news' => 'bool',
            'is_mail_forum_news' => 'bool',
            'is_mail_answer_entity' => 'bool',
        ];
    }
}
