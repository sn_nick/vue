<?php

namespace App\Exceptions\Custom;

use Symfony\Component\HttpKernel\Exception\HttpException;

class CustomException extends HttpException
{
    public function __construct(int $status, string $message, \Throwable $previous = null, int $code = 0, array $headers = [])
    {
        parent::__construct($status, $message, $previous, $headers, $code);
    }
}
