<?php
declare(strict_types=1);

namespace App\Components\Repository;

use App\Components\Repository\Contracts\IndexContract;
use App\Components\Repository\Contracts\StoreContract;
use App\Models\Comment;
use App\Models\Status;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Components\QueryFilter\QueryFilterContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class CommentRepository extends Repository implements IndexContract, StoreContract
{
    public function index(QueryFilterContract $filters): LengthAwarePaginator
    {
        return Comment::with('user.profile')->filter($filters)->paginate();
    }

    public function store(array $params): Comment
    {
        $params['user_id'] = Auth::id();

        $comment = Comment::create($params);
        $comment->setStatus(Status::ACCEPTED_STATUS);

        $comment->load('user.profile');

        return $comment;
    }
}
