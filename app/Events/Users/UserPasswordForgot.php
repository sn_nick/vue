<?php
declare(strict_types=1);

namespace App\Events\Users;

use App\Components\DTO\Users\UserRegisteredDTO;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserPasswordForgot
{
    use Dispatchable;
    use InteractsWithSockets;
    use SerializesModels;

    public User $user;
    public UserRegisteredDTO $dto;

    /**
     * Create a new event instance.
     *
     * @param UserRegisteredDTO $dto
     * @param User $user
     */
    public function __construct(User $user, UserRegisteredDTO $dto)
    {
        $this->user = $user;
        $this->dto = $dto;
    }
}
