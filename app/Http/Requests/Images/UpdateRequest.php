<?php
declare(strict_types=1);

namespace App\Http\Requests\Images;

use App\Traits\RequestParams;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    use RequestParams;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return (bool)Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->setAllowedParams([
                'image_category_id',
            ]
        );

        return [
            'image_category_id' => ['required', 'integer', 'exists:image_categories,id'],
            'image' => ['required', 'integer', 'exists:images,id'],
        ];
    }
}
