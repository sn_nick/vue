<?php
declare(strict_types=1);

namespace App\Listeners\Articles;

use App\Events\Articles\ArticleCreated;
use App\Mail\Articles\{AddNewArticleForBlog, AddNewArticleForForum};
use App\Models\{ArticleType, User};
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Mail;

class AddNewArticleSendMail
{
    private ArticleCreated $event;

    /**
     * Handle the event.
     *
     * @param ArticleCreated $event
     * @return void
     */
    public function handle(ArticleCreated $event): void
    {
        $this->event = $event;

        $this->getUsersForSendMail()->map(function ($user) {
            $this->checkSendMailUserForForum($user);
            $this->checkSendMailUserForBlog($user);
        });
    }

    private function getUsersForSendMail(): Collection
    {
        return (new User())
            ->with('profile')
            ->where('users.id', '!=', $this->event->article->user_id)
            ->get();
    }

    private function checkSendMailUserForForum(User $user): void
    {
        if ($user->profile->is_mail_forum_news && $this->event->article->article_type_id === ArticleType::FORUM_ID) {
            $this->sendMailUserForForum($user);
        }
    }

    private function checkSendMailUserForBlog(User $user): void
    {
        if ($user->profile->is_mail_blog_news && $this->event->article->article_type_id === ArticleType::BLOG_ID) {
            $this->sendMailUserForBlog($user);
        }
    }

    private function sendMailUserForForum(User $user): void
    {
        Mail::to($user)
            ->send(
                (new AddNewArticleForForum($this->event->article, $user))
                    ->onQueue(\config('queue.send_mail'))
            );
    }

    private function sendMailUserForBlog(User $user): void
    {
        Mail::to($user)
            ->send(
                (new AddNewArticleForBlog($this->event->article, $user))
                    ->onQueue(\config('queue.send_mail'))
            );
    }
}
