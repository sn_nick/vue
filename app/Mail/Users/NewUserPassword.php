<?php
declare(strict_types=1);

namespace App\Mail\Users;

use App\Components\DTO\Users\UserRegisteredDTO;
use Illuminate\Support\Facades\Log;
use Throwable;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewUserPassword extends Mailable
{
    use Queueable;
    use SerializesModels;

    public UserRegisteredDTO $dto;

    /**
     * Create a new message instance.
     *
     * @param UserRegisteredDTO $dto
     */
    public function __construct(UserRegisteredDTO $dto)
    {
        $this->dto = $dto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->from(\config('mail.from.address'))
            ->view('mail.users.new-user-password', ['password' => $this->dto->getPassword()])
            ->subject('Новый пароль');
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        Log::error('Error send mail new user password', [$exception]);
    }
}
