<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Sex extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function profiles(): HasMany
    {
        return $this->hasMany(Profile::class);
    }
}
