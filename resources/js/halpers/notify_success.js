export default function (obj, text = '') {
    obj.$notify({
        group: 'success',
        title: 'Success',
        speed: 1500,
        text: text ? text : 'Операция прошла успешно'
    });
}

