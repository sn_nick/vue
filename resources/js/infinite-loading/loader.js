import Vue from 'vue'
import Resource from 'vue-resource'
import Loader from 'vue-infinite-loading'

Vue.use(Resource)
Vue.use(Loader)

export default Loader;
