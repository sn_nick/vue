<?php
declare(strict_types=1);

namespace App\Http\Resources\Articles;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticlesResource extends JsonResource
{
    use ResourceWith;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var \App\Models\Article $this */

        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'img' => $this->img,
            'description' => $this->description,
            'like' => $this->like,
            'view' => $this->view,
            'created_at' => $this->created_at,
            'user' => $this->user,
            'article_category' => $this->articleCategory,
            'comments' => $this->comments,
            'synthetic' => $this->synthetic,
        ];
    }
}
