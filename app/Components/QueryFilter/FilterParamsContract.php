<?php
declare(strict_types=1);

namespace App\Components\QueryFilter;

/**
 * Interface FilterParamsContract
 * @package App\Components\QueryFilter
 */
interface FilterParamsContract
{
    public const TYPE_FILTER = 'filter';
    public const TYPE_SORT = 'sort';

    /**
     * @return array
     */
    public function filter(): array;

    /**
     * @return string
     */
    public function sort(): string;

    /**
     * @param string $type
     * @param        $value
     */
    public function setParams(string $type, $value): void;
}
