export default {
    IMAGES_HOME(state){
        return state.images.home
    },
    IMAGES_CATALOG(state){
        return state.images.catalog
    },
    IMAGES_SIMILAR(state){
        return state.images.similar
    },
    IMAGE(state){
        return state.images.image
    },
    IMAGES_FILTERS(state){
        return state.images.url_params.filters
    },
    IMAGES_URL_PARAMS(state){
        return state.images.url_params
    },
    IMAGES_PAGE(state){
        return state.images.url_params.page
    },
    IMAGES_SORT(state){
        return state.images.url_params.sort
    },
    IMAGES_DASHBOARD_PAGE(state){
        return state.images.dashboard.url_params.page
    },

    ARTICLES_CATALOG(state){
        return state.articles.catalog
    },
    ARTICLES_FORUM(state){
        return state.articles.forum
    },
    ARTICLES_BLOG(state){
        return state.articles.blog
    },
    ARTICLE(state){
        return state.articles.article
    },
    ARTICLES_FILTERS(state){
        return state.articles.url_params.filters
    },
    ARTICLES_DASHBOARD_FILTERS(state){
        return state.articles.dashboard.url_params.filters
    },
    ARTICLES_URL_PARAMS(state){
        return state.articles.url_params
    },
    ARTICLES_FORUM_PAGE(state){
        return state.articles.url_params.forum_page
    },
    ARTICLES_BLOG_PAGE(state){
        return state.articles.url_params.blog_page
    },
    ARTICLES_DASHBOARD_PAGE(state){
        return state.articles.dashboard.url_params.page
    },

    COMMENTS_CATALOG(state){
        return state.comments.catalog
    },
    COMMENTS_FILTERS(state){
        return state.comments.url_params.filters
    },
    COMMENTS_URL_PARAMS(state){
        return state.comments.url_params
    },
    COMMENTS_PAGE(state){
        return state.comments.url_params.page
    },

    DICTIONARIES(state){
        return state.dictionaries
    },

}
