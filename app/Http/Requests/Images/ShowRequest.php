<?php
declare(strict_types=1);

namespace App\Http\Requests\Images;

use App\Traits\RequestFilter;
use App\Traits\RequestParams;
use Illuminate\Foundation\Http\FormRequest;

class ShowRequest extends FormRequest
{
    use RequestParams;
    use RequestFilter;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [];
    }
}
