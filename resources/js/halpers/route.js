const routes = require('../router/routes.json');

//route('home', ['1'])
export default function () {
    const args = Array.prototype.slice.call(arguments);

    const name = args.shift();

    if(routes[name] === undefined){
        console.log('Error');
    } else {
        return '/' + routes[name]
            .split('/')
            .map(str => str[0] === '{' ? args.shift() : str)
            .join('/');
    }
}

