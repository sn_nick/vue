export default {
    data: function () {
        return {
            page: 2,
            lastPage: 0,
        }
    },

    methods: {
        infiniteHandler: function (state, type) {
            console.log(type)
            setTimeout(function () {
                axios.get(`${type}?page=${this.page}`)
                    .then(response => {
                        if (response.data[type].length > 0) {
                            this.lastPage = response.data.meta.last_page;

                            this.$store.commit(type.toUpperCase(), response.data[type]);

                            if (this.page - 1 === this.lastPage) {
                                this.page = 2;
                                state.complete();
                            } else {
                                this.page += 1;
                            }

                            state.loaded();
                        } else {
                            this.page = 2;
                            state.complete();
                        }
                    })
                    .catch(e => console.log(e));
            }.bind(this, state), 1000);
        },
    },
}