<?php
declare(strict_types=1);

namespace App\Http\Requests\Profiles;

use App\Components\Validator\Profiles\ChangePassword;
use App\Components\Validator\ValidatorService;
use App\Traits\RequestParams;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Validator;

class ChangePasswordRequest extends FormRequest
{
    use RequestParams;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return (bool)Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->setAllowedParams([
                'password',
            ]
        );

        return [
            'old_password' => 'required',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ];
    }

    public function withValidator(Validator $validator): void
    {
        $service = new ValidatorService(new ChangePassword());
        $service->afterValidate($this, $validator);
    }
}
