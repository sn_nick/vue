<?php
declare(strict_types=1);

namespace App\Components\QueryFilter;

interface QueryFilterContract
{
    /** @var string  */
    public const SORT_ASC = 'asc';
    /** @var string  */
    public const SORT_DESC = 'desc';

    public function apply($builder);
}
