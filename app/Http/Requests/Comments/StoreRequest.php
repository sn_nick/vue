<?php
declare(strict_types=1);

namespace App\Http\Requests\Comments;

use App\Traits\RequestParams;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class StoreRequest extends FormRequest
{
    use RequestParams;

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return (bool)Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $this->setAllowedParams(['name', 'description', 'entity_type_id', 'entity_type', 'entity_id']);

        return [
            'name' => ['required', 'string'],
            'description' => ['required', 'string'],
            'entity_type_id' => ['required', 'integer', 'exists:entity_types,id'],
            'entity_type' => ['required', 'string', 'exists:entity_types,name'],
            'entity_id' => ['required', 'integer'],
        ];
    }
}
