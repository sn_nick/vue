<?php
declare(strict_types=1);

namespace App\Components\Validator;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

class ValidatorService
{
    private StrategyContract $strategy;

    public function __construct(StrategyContract $strategy)
    {
        $this->strategy = $strategy;
    }

    public function setStrategy(StrategyContract $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function afterValidate(Request $request, Validator $validator): void
    {
        $this->strategy->afterValidate($request, $validator);
    }
}