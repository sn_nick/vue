<?php
declare(strict_types=1);

namespace App\Components\Repository;

use App\Components\Repository\Contracts\IndexContract;
use App\Components\Repository\Contracts\ShowContract;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Components\QueryFilter\QueryFilterContract;
use Illuminate\Database\Eloquent\Model;

class UserRepository extends Repository implements IndexContract, ShowContract
{
    public function index(QueryFilterContract $filters): LengthAwarePaginator
    {
        return User::filter($filters)->paginate();
    }

    public function show(QueryFilterContract $filters, array $params = []): Model
    {
        return User::with(['profile'])
                   ->where('id', $params['user'])
                   ->firstOrFail();
    }
}
