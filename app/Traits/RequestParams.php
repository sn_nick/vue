<?php

namespace App\Traits;

use App\Exceptions\Custom\CustomException;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;

trait RequestParams
{
    protected array $allowedParams = [];

    /**
     * @param null $keys
     * @return array
     */
    public function all($keys = null): array
    {
        return array_merge(parent::all($keys), $this->getUrlParams());
    }

    /**
     * @return array
     */
    public function getRequestParams(): array
    {
        if ($this->getAllowedParams() && count($this->getAllowedParams()) > 0) {
            return request()->only($this->getAllowedParams());
        }

        return request()->all();
    }

    /**
     * @return array
     */
    public function getUrlParams(): array
    {
        $route = request()->route();

        if ($route === null) {
            throw new CustomException(Response::HTTP_CONFLICT, __('validation.custom.user.user_type'));
        }

        return $route->parameters();
    }

    /**
     * @return array
     */
    public function getAllRequestParams(): array
    {
        return array_merge($this->getUrlParams(), $this->getRequestParams());
    }

    /**
     * @return Collection
     */
    public function getRequestFiles(): Collection
    {
        return $this->hasFile('files') ? collect($this->file('files')) : collect();
    }

    /**
     * @return array
     */
    protected function getAllowedParams(): array
    {
        return $this->allowedParams;
    }

    /**
     * @param array $allowedParams
     */
    protected function setAllowedParams(array $allowedParams): void
    {
        $this->allowedParams = $allowedParams;
    }
}
