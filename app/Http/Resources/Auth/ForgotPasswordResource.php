<?php
declare(strict_types=1);

namespace App\Http\Resources\Auth;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ForgotPasswordResource extends JsonResource
{
    use ResourceWith;

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        if (empty($this->resource)) {
            return [];
        }

        return parent::toArray($request);
    }
}
