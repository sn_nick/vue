<?php
declare(strict_types=1);

namespace App\Models;

use App\Traits\ScopeFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Site extends Model
{
    use ScopeFilter;

    /**
     * @var string[]
     */
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }
}
