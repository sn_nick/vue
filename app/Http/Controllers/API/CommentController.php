<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Components\QueryFilter\Comments\CommentsIndexFilter;
use App\Components\Repository\CommentRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Comments\IndexRequest;
use App\Http\Requests\Comments\StoreRequest;
use App\Http\Resources\Comments\CommentsResource;
use App\Http\Resources\Comments\CommentsResourceCollection;

class CommentController extends Controller
{
    private CommentRepository $repository;

    /**
     * CommentController constructor.
     * @param CommentRepository $repository
     */
    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return CommentsResourceCollection
     */
    public function index(IndexRequest $request): CommentsResourceCollection
    {
        $filters = new CommentsIndexFilter($request->getFilterParams());
        $comments = $this->repository->index($filters);

        return new CommentsResourceCollection($comments);
    }

    /**
     * @param StoreRequest $request
     * @return CommentsResource
     */
    public function store(StoreRequest $request): CommentsResource
    {
        $params = $request->getRequestParams();
        $comment = $this->repository->store($params);
        
        return new CommentsResource($comment);
    }
}
