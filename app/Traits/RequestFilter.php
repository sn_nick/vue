<?php

namespace App\Traits;

use App\Components\QueryFilter\FilterParams;
use JsonException;

trait RequestFilter
{
    public function getFilterParams(): ?FilterParams
    {
        try {
            return new FilterParams(
                [
                    'filter' => $this->getFilters(),
                    'sort'   => $this->getSort(),
                ]
            );
        } catch (JsonException $e) {
            throw new \LogicException('Error filter params');
        }
    }

    protected function getSort(): ?string
    {
        if (request()->has('sort')) {
            return request()->input('sort');
        }

        return null;
    }

    /**
     * @return mixed
     * @throws JsonException
     */
    protected function getFilters(): ?array
    {
        if (request()->has('filters')) {
            return json_decode(request()->input('filters'), true, 512, JSON_THROW_ON_ERROR);
        }

        return null;
    }
}
