export default {
    SET_IMAGES_HOME: (state, images) => {
        state.images.home = images;
    },
    SET_IMAGES_CATALOG: (state, images) => {
        state.images.catalog = images;
    },
    SET_IMAGES_SIMILAR: (state, images) => {
        state.images.similar = images;
    },
    SET_IMAGES_FILTERS: (state, filters) => {
        if(Object.keys(state.images.url_params.filters).length > 0){
            filters = {...state.images.url_params.filters, ...filters};
        }
        state.images.url_params.filters = filters;
    },
    SET_IMAGES_URL_PARAMS: (state, url_params) => {
        state.images.url_params = url_params;
    },
    SET_IMAGES_PAGE: (state, page) => {
        state.images.url_params.page = page;
    },
    SET_IMAGES_SORT: (state, sort) => {
        state.images.url_params.sort = sort;
    },
    SET_IMAGE: (state, image) => {
        state.images.image = image;
    },
    SET_IMAGES_DASHBOARD_PAGE: (state, page) => {
        state.images.dashboard.url_params.page = page;
    },

    SET_ARTICLES_CATALOG: (state, articles) => {
        state.articles.catalog = articles;
    },
    SET_ARTICLES_FORUM: (state, articles) => {
        state.articles.forum = articles;
    },
    SET_ARTICLES_DASHBOARD: (state, articles) => {
        state.articles.dashboard.articles = articles;
    },
    SET_ARTICLES_BLOG: (state, articles) => {
        state.articles.blog = articles;
    },
    SET_ARTICLE: (state, article) => {
        state.articles.article = article;
    },
    SET_ARTICLES_URL_PARAMS: (state, url_params) => {
        state.articles.url_params = url_params;
    },
    SET_ARTICLES_FORUM_PAGE: (state, page) => {
        state.articles.url_params.forum_page = page;
    },
    SET_ARTICLES_BLOG_PAGE: (state, page) => {
        state.articles.url_params.blog_page = page;
    },
    SET_ARTICLES_DASHBOARD_PAGE: (state, page) => {
        state.articles.dashboard.url_params.page = page;
    },
    SET_ARTICLES_FILTERS: (state, filters) => {
        if(Object.keys(state.articles.url_params.filters).length > 0){
            filters = {...state.articles.url_params.filters, ...filters};
        }
        state.articles.url_params.filters = filters;
    },
    SET_ARTICLES_DASHBOARD_FILTERS: (state, filters) => {
        if(Object.keys(state.articles.url_params.filters).length > 0){
            filters = {...state.articles.url_params.filters, ...filters};
        }
        state.articles.dashboard.url_params.filters = filters;
    },

    SET_COMMENTS_CATALOG: (state, comments) => {
        state.comments.catalog = comments;
    },
    SET_COMMENTS_URL_PARAMS: (state, url_params) => {
        state.comments.url_params = url_params;
    },
    SET_COMMENTS_PAGE: (state, page) => {
        state.comments.url_params.page = page;
    },
    SET_COMMENTS_FILTERS: (state, filters) => {
        if(Object.keys(state.comments.url_params.filters).length > 0){
            filters = {...state.comments.url_params.filters, ...filters};
        }
        state.comments.url_params.filters = filters;
    },

    SET_DICTIONARIES: (state, dictionaries) => {
        state.dictionaries = dictionaries;
    },
    SET_HANDLER: (state, handler) => {
        state.handler = handler;
    },
}
