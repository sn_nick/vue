import {mapActions} from "vuex";

export default {
    methods: {
        ...mapActions([
            'GET_DICTIONARIES',
            'ADD_IMAGES_PAGE',
            'ADD_IMAGES_FILTERS',
        ]),
        search_filter(search) {
            let filters = {search: search}
            this.filters(filters)
        },
        image_category_filter(image_category_id) {
            let filters = {image_category_id: image_category_id}
            this.filters(filters)
        },
        filters(filters) {
            this.ADD_IMAGES_PAGE(1)
            this.ADD_IMAGES_FILTERS(filters)
            this.$store.state.images.catalog = [];
            if(this.$route.name !== 'catalog'){
                this.$router.push('/catalog')
            }
        },
    }
}