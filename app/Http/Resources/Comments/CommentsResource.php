<?php
declare(strict_types=1);

namespace App\Http\Resources\Comments;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentsResource extends JsonResource
{
    use ResourceWith;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var \App\Models\Comment $this */

        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'user' => $this->user,
        ];
    }
}
