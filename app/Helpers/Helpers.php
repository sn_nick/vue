<?php
declare(strict_types=1);

if (!function_exists('base64_url_encode'))
{
    function base64_url_encode($data)
    {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }
}

if (!function_exists('base64_url_decode'))
{
    function base64_url_decode($data)
    {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }
}

if (!function_exists('get_dir_img'))
{
    function get_dir_img($model){
        $site = $model->site->name ?? 'uploaded';
        $filename = $model->img;
        $dir = request()->root()."/storage/img/$site";
        $dir .= '/'.mb_substr($filename,0,1,"UTF-8");
        $dir .= '/'.mb_substr($filename,1,2,"UTF-8");
        $dir .= '/'.$filename;

        return $dir;
    }
}

if (!function_exists('get_dir_upload'))
{
    function get_dir_upload($filename){
        $dir = request()->root()."/storage/img/uploaded";
        $dir .= '/'.mb_substr($filename,0,1,"UTF-8");
        $dir .= '/'.mb_substr($filename,1,2,"UTF-8");
        $dir .= '/'.$filename;

        return $dir;
    }
}

if (!function_exists('get_contents_recursive'))
{
    // Рекурсия ищет все файлы в папке
    function get_contents_recursive($dir, &$results = [])
    {
        $files = scandir($dir);
        foreach($files as $key => $value){
            $path = realpath($dir.DIRECTORY_SEPARATOR.$value);
            if(!is_dir($path)) {
                if (preg_match(config('constants.pattern_img'), $path)){
                    $results[] = $path;
                }
            } else if($value !== "." && $value !== "..") {
                get_contents_recursive($path, $results);
                //$results[] = $path;
            }
        }
        return $results;
    }
}

if (!function_exists('get_random_filename'))
{
    function get_random_filename(): string
    {
        return \md5((string)\microtime(true));
    }
}

function transliteration($string) {
    //@formatter:off
    $converter = [
        'а' => 'a',  'б' => 'b',  'в' => 'v',   'г' => 'g',  'д' => 'd', 'е' => 'e',  'ё' => 'e', 'ж' => 'zh',
        'з' => 'z',  'и' => 'i',  'й' => 'y',   'к' => 'k',  'л' => 'l', 'м' => 'm',  'н' => 'n', 'о' => 'o',
        'п' => 'p',  'р' => 'r',  'с' => 's',   'т' => 't',  'у' => 'u', 'ф' => 'f',  'х' => 'h', 'ц' => 'c',
        'ч' => 'ch', 'ш' => 'sh', 'щ' => 'sch', 'ь' => '\'', 'ы' => 'y', 'ъ' => '\'', 'э' => 'e', 'ю' => 'yu',
        'я' => 'ya',

        'А' => 'A',  'Б' => 'B',  'В' => 'V',   'Г' => 'G',  'Д' => 'D', 'Е' => 'E',  'Ё' => 'E', 'Ж' => 'Zh',
        'З' => 'Z',  'И' => 'I',  'Й' => 'Y',   'К' => 'K',  'Л' => 'L', 'М' => 'M',  'Н' => 'N', 'О' => 'O',
        'П' => 'P',  'Р' => 'R',  'С' => 'S',   'Т' => 'T',  'У' => 'U', 'Ф' => 'F',  'Х' => 'H', 'Ц' => 'C',
        'Ч' => 'Ch', 'Ш' => 'Sh', 'Щ' => 'Sch', 'Ь' => '\'', 'Ы' => 'Y', 'Ъ' => '\'', 'Э' => 'E', 'Ю' => 'Yu',
        'Я' => 'Ya',
    ];
    //@formatter:on
    // переводим в транслит
    $str = strtr($string, $converter);
    // в нижний регистр
    $str = strtolower($str);
    // заменям все ненужное нам на "-"
    $str = preg_replace('~[^-a-z0-9_]+~u', '-', $str);
    // удаляем начальные и конечные '-'
    $str = trim($str, '-');

    return $str;
}
