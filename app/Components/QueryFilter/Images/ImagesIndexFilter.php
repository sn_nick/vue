<?php
declare(strict_types=1);

namespace App\Components\QueryFilter\Images;

use App\Components\QueryFilter\QueryFilter;
use App\Models\ImageOrientation;
use App\Models\Status;
use Illuminate\Database\Eloquent\Builder;

class ImagesIndexFilter extends QueryFilter
{
    public function apply($builder): Builder
    {
        return parent::apply($builder)->currentStatus(Status::ACCEPTED_STATUS);
    }

    public function default(): Builder
    {
        return $this->builder->where('image_orientation_id', ImageOrientation::HORIZONTAL_ID);
    }

    public function image_category_id(int $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->where('image_category_id', $value);
    }

    public function image_orientation_id(int $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->where('image_orientation_id', $value);
    }

    public function search(string $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        $table = $this->table;
        return $this->builder->select("{$table}.*")->join(
            'image_categories',
            static function ($join) use ($table) {
                $join->on("{$table}.image_category_id", '=', 'image_categories.id');
            }
        )->where(
            static function ($query) use ($value, $table) {
                $query->where("{$table}.name", 'like', "$value%")
                      ->orWhere("{$table}.slug", 'like', "%$value%")
                      ->orWhere('image_categories.name', 'like', "%$value%");
            }
        );
    }

    public function sort_view(string $table, string $column, string $order): Builder
    {
        if(empty($table)){
            $table = $this->table;
        }
        return $this->builder->orderBy("$table.$column", $order);
    }

    public function sort_like(string $table, string $column, string $order): Builder
    {
        if(empty($table)){
            $table = $this->table;
        }
        return $this->builder->orderBy("$table.$column", $order);
    }

    public function sort_load(string $table, string $column, string $order): Builder
    {
        if(empty($table)){
            $table = $this->table;
        }
        return $this->builder->orderBy("$table.$column", $order);
    }

    public function sort_created_at(string $table, string $column, string $order): Builder
    {
        if(empty($table)){
            $table = $this->table;
        }
        return $this->builder->orderBy("$table.$column", $order);
    }
}