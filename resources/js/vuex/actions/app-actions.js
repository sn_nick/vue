export default {
    ADD_HANDLER({commit}, handler){
        commit('SET_HANDLER', handler);
    },
    ADD_IMAGES_URL_PARAMS({commit}, url_params){
        commit('SET_IMAGES_URL_PARAMS', url_params);
    },
    ADD_IMAGES_FILTERS({commit}, filters){
        commit('SET_IMAGES_FILTERS', filters);
    },
    ADD_IMAGES_PAGE({commit}, page){
        commit('SET_IMAGES_PAGE', page);
    },
    ADD_IMAGES_SORT({commit}, sort){
        commit('SET_IMAGES_SORT', sort);
    },
    ADD_IMAGES_DASHBOARD_PAGE({commit}, page){
        commit('SET_IMAGES_DASHBOARD_PAGE', page);
    },

    ADD_ARTICLES_URL_PARAMS({commit}, url_params){
        commit('SET_ARTICLES_URL_PARAMS', url_params);
    },
    ADD_ARTICLES_FILTERS({commit}, filters){
        commit('SET_ARTICLES_FILTERS', filters);
    },
    ADD_ARTICLES_DASHBOARD_FILTERS({commit}, filters){
        commit('SET_ARTICLES_DASHBOARD_FILTERS', filters);
    },
    ADD_ARTICLES_FORUM_PAGE({commit}, page){
        commit('SET_ARTICLES_FORUM_PAGE', page);
    },
    ADD_ARTICLES_BLOG_PAGE({commit}, page){
        commit('SET_ARTICLES_BLOG_PAGE', page);
    },
    ADD_ARTICLES_DASHBOARD_PAGE({commit}, page){
        commit('SET_ARTICLES_DASHBOARD_PAGE', page);
    },
    ADD_COMMENTS_URL_PARAMS({commit}, url_params){
        commit('SET_COMMENTS_URL_PARAMS', url_params);
    },
    ADD_COMMENTS_FILTERS({commit}, filters){
        commit('SET_COMMENTS_FILTERS', filters);
    },
    ADD_COMMENTS_PAGE({commit}, page){
        commit('SET_COMMENTS_PAGE', page);
    },
}
