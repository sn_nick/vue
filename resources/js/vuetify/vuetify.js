import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css';
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify);

const vuetify = new Vuetify({
    icons: {
        iconfont: 'mdi',
        values: {
            delete: 'mdi-delete',
            view_headline: 'mdi-view-headline',
            chevron_down: 'mdi-chevron-down',
            web: 'mdi-web',
            facebook: 'mdi-facebook',
            instagram: 'mdi-instagram',
            twitter: 'mdi-twitter',
            magnify: 'mdi-magnify',
            thumb_up: 'mdi-thumb-up',
            star: 'mdi-star',
            share_variant: 'mdi-share-variant',
            cloud_upload: 'mdi-cloud-upload',
            eye_outline: 'mdi-eye-outline',
            comment_outline: 'mdi-comment-outline',
            chevron_down_circle_outline: 'mdi-chevron-down-circle-outline',
            image_edit_outline: 'mdi-image-edit-outline',
            delete_outline: 'mdi-delete-outline',
        },
    }
});

export default vuetify
