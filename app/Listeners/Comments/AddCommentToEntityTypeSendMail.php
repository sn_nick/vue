<?php

declare(strict_types=1);

namespace App\Listeners\Comments;

use App\Events\Comments\CommentCreated;
use App\Exceptions\Custom\CustomException;
use App\Mail\Comments\AddCommentToEntityType;
use Illuminate\Support\Facades\{Auth, Mail};
use Symfony\Component\HttpFoundation\Response;

class AddCommentToEntityTypeSendMail
{
    /**
     * Handle the event.
     *
     * @param CommentCreated $event
     * @return void
     */
    public function handle(CommentCreated $event): void
    {
        $entity = $event->comment->entity()->with(['user.profile'])->first();

        if ($entity === null) {
            throw new CustomException(Response::HTTP_CONFLICT, 'Entity not found');
        }

        if ($this->checkMailSend($entity)) {
            Mail::to($entity->user)
                ->send(
                    (new AddCommentToEntityType($entity))
                        ->onQueue(\config('queue.send_mail'))
                );
        }
    }

    private function checkMailSend($entity): bool
    {
        return $entity->user->profile->is_mail_answer_entity && $entity->user_id !== Auth::id();
    }
}
