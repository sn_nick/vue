<?php
declare(strict_types=1);

namespace App\Http\Resources\Users;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    use ResourceWith;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request): array
    {
        /** @var \App\Models\User $this */

        return [
            'id' => $this->id,
            'name' => $this->name,
            'img' => $this->img,
        ];
    }
}
