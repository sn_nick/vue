<?php
declare(strict_types=1);

namespace App\Components\Validator;

use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

interface StrategyContract
{
    public function afterValidate(Request $request, Validator $validator): void;
}