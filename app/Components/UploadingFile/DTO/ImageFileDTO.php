<?php
declare(strict_types=1);

namespace App\Components\UploadingFile\DTO;

use App\Components\UploadingFile\Contracts\FileDTOContract;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\File\File;

class ImageFileDTO implements FileDTOContract
{
    private Collection $imageCollection;
    private int $imageCategoryId;
    private int $imageOrientationId;
    private Collection $imageFiles;

    public function __construct(int $imageCategoryId, $imageFiles)
    {
        $this->imageCollection = new Collection();
        $this->imageFiles = $imageFiles;
        $this->imageCategoryId = $imageCategoryId;
    }

    public function setImageOrientationId(int $imageOrientationId): void
    {
        $this->imageOrientationId = $imageOrientationId;
    }

    public function getImageOrientationId(): int
    {
        return $this->imageOrientationId;
    }

    public function getImageFiles(): Collection
    {
        return $this->imageFiles;
    }

    public function getImageCategoryId(): int
    {
        return $this->imageCategoryId;
    }

    public function addImage(UploadedFile $uploaded_file, File $file): void
    {
        $this->imageCollection->push([
            'name' => $uploaded_file->getClientOriginalName(),
            'slug' => transliteration($uploaded_file->getClientOriginalName() . '-' . $file->getFileName()),
            'img' => $file->getFileName(),
            'old_link' => '',
            'image_orientation_id' => $this->getImageOrientationId(),
            'image_category_id' => $this->getImageCategoryId(),
            'user_id' => Auth::id(),
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }

    public function getImageCollection(): Collection
    {
        return $this->imageCollection;
    }
}
