<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create(
            'profiles',
            static function (Blueprint $table) {
                $table->id();
                $table->bigInteger('user_id');
                $table->string('full_name')->default('');
                $table->longText('img')->default('');
                $table->unsignedSmallInteger('sex_id')->nullable();
                $table->date('date_birth')->nullable();
                $table->text('description')->default('');
                $table->string('facebook')->default('');
                $table->string('instagram')->default('');
                $table->boolean('is_mail_blog_news')->default(false);
                $table->boolean('is_mail_forum_news')->default(false);
                $table->boolean('is_mail_answer_entity')->default(false);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('profiles');
    }
}
