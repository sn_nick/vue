<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Helpers\Time;
use App\Http\Controllers\Controller;
use App\Models\{ArticleCategory, ArticleType, EntityType, ImageCategory, ImageOrientation, Sex, Site, Sort};
use Illuminate\Support\Facades\Cache;
use Illuminate\Http\JsonResponse;

class DictionaryController extends Controller
{
    public function __invoke(): JsonResponse
    {
        $dictionaries = Cache::remember(
            'dictionaries',
            Time::DAY,
            function () {
                return [
                    'sites' => Site::all()->toArray(),
                    'article_types' => ArticleType::all()->toArray(),
                    'article_categories' => ArticleCategory::all()->toArray(),
                    'entity_types' => EntityType::all()->toArray(),
                    'image_categories' => ImageCategory::all()->toArray(),
                    'sorts' => Sort::all()->toArray(),
                    'image_orientations' => ImageOrientation::all()->toArray(),
                    'sexes' => Sex::all()->toArray(),
                ];
            }
        );

        return response()->json(['dictionaries' => $dictionaries]);
    }
}
