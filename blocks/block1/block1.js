"use strict";

function toggleHeaderMenu(elem) {
    let headerMenu = elem.parentNode.children[1];
    headerMenu.style.display = headerMenu.style.display === 'none' ? 'flex' : 'none';
}

function toggleLanguageMenu(elem) {
    let languageMenu = elem.parentNode.children[1];
    languageMenu.style.display = languageMenu.style.display === 'none' ? 'flex' : 'none';
}

function toggleHeaderMenuMobile(elem) {
    let headerMenu = elem.parentNode.children[0].children[1];
    headerMenu.style.display = headerMenu.style.display === 'none' ? 'flex' : 'none';
}