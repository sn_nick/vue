<?php
declare(strict_types=1);

namespace App\Mail\Users;

use App\Components\DTO\Users\UserRegisteredDTO;
use Illuminate\Support\Facades\Log;
use Throwable;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisteredUser extends Mailable
{
    use Queueable;
    use SerializesModels;

    public UserRegisteredDTO $dto;

    /**
     * Create a new message instance.
     *
     * @param UserRegisteredDTO $dto
     */
    public function __construct(UserRegisteredDTO $dto)
    {
        $this->dto = $dto;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->from(\config('mail.from.address'))
            ->view('mail.registration-confirm', ['token' => $this->dto->getToken(), 'password' => $this->dto->getPassword()])
            ->subject('Подтверждение регистрации');
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        Log::error('Error send mail registration user', [$exception]);
    }
}
