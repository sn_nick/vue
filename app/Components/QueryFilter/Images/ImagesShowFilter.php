<?php
declare(strict_types=1);

namespace App\Components\QueryFilter\Images;

use App\Components\QueryFilter\QueryFilter;
use App\Models\Status;
use Illuminate\Database\Eloquent\Builder;

class ImagesShowFilter extends QueryFilter
{
    public function apply($builder): Builder
    {
        return parent::apply($builder)->currentStatus(Status::ACCEPTED_STATUS);
    }

    public function default(): Builder
    {
        return $this->builder;
    }
}