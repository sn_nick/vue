# Image forum on vue js on MACOS
###Запустить команды:
make up
make imageportal-init

# Image forum on vue js on UBUNTU
## установить docker (из официального хранилища не из snap)
## установить docker-compose

###Запустить команду:
sudo nano /etc/hosts

###Вставить в файл эти две строки:
128.20.0.1	imagesportal.docker

128.20.0.1	api.imagesportal.docker

###Запустить команду что ниже и следовать инструкциям.
cd docker/ubuntu ; ./install.sh ; cd ..

Перейти по ссылке: http://imagesportal.docker/


###Управление контейнерами после сброрки проекта из папки docker
./docker.sh up - Поднять контейнеры

./docker.sh down - Потушить контейнеры

./docker.sh restart - Перезапустить контейнеры
