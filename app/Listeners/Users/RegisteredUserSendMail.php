<?php
declare(strict_types=1);

namespace App\Listeners\Users;

use App\Events\Users\UserRegistered;
use App\Mail\Users\RegisteredUser;
use Illuminate\Support\Facades\Mail;

class RegisteredUserSendMail
{
    /**
     * Handle the event.
     *
     * @param UserRegistered $event
     * @return void
     */
    public function handle(UserRegistered $event): void
    {
        Mail::to($event->user)->send(
            (new RegisteredUser($event->dto))->onQueue(\config('queue.send_mail'))
        );
    }
}
