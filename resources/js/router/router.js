import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Images from '../components/images/v-images';
import Catalog from '../components/page/v-catalog-images';
import Home from '../components/page/v-home';
import Blog from '../components/page/v-blog';
import Forum from '../components/page/v-forum';
import Image from '../components/page/v-catalog-image';
import Article from '../components/page/v-forum-article';
import Confirm from '../components/page/v-confirm';
import DashboardProfilePage from '../components/dashboard/page/v-dashboard-profile-page';
import DashboardForumPage from '../components/dashboard/page/v-dashboard-forum-page';
import DashboardBlogPage from '../components/dashboard/page/v-dashboard-blog-page';
import DashboardCatalogImagesPage from '../components/dashboard/page/v-dashboard-catalog-images-page';
import Register from '../components/auth/v-register';
import Login from '../components/auth/v-login';
import route from "../halpers/route";

export default new VueRouter({
    routes : [
        //TODO web
        {
            path : '/',
            name: 'home',
            component : Home,
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path : '/catalog',
            name: 'catalog',
            component : Catalog,
        },
        {
            path : '/blog',
            name: 'blog',
            component : Blog,
        },
        {
            path : '/forum',
            name: 'forum',
            component : Forum,
        },
        {
            path : '/image/:slug',
            name: 'catalog_image',
            component : Image,
        },
        {
            path : '/article/:slug',
            name: 'forum_article',
            component : Article,
        },
        {
            path : '/confirm/:token',
            name: 'confirm',
            component : Confirm,
        },
        //TODO auth
        {
            path: '/dashboard',
            name: 'dashboard',
            component: DashboardProfilePage,
            meta: {
                auth: true
            }
        },
        {
            path: '/auth/catalog',
            name: 'auth.catalog',
            component: DashboardCatalogImagesPage,
            meta: {
                auth: true
            }
        },
        {
            path: '/auth/forum',
            name: 'auth.forum',
            component: DashboardForumPage,
            meta: {
                auth: true
            }
        },
        {
            path: '/auth/blog',
            name: 'auth.blog',
            component: DashboardBlogPage,
            meta: {
                auth: true
            }
        },

        //TODO api
        {
            path : 'images.index',
            name : 'images.index',
            component : Images,
        },
        {
            path : 'images.show',
            name : 'images.show',
            component : Image,
        },
        {
            path : 'images.store',
            name : 'images.store',
            component : Image,
            meta: {
                auth: true
            }
        },
        {
            path : 'images.update',
            name : 'images.update',
            component : Image,
            meta: {
                auth: true
            }
        }
    ],
    mode : 'history'
});
