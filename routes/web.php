<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Site'], static function () {

    Route::get('confirm/{token}', [
        'as' => 'confirm',
        'uses' => 'AuthController@confirm'
    ]);

    Route::get('/{page?}/{param?}/{slug?}', 'HomeController@index');
});

