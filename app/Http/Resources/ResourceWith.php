<?php
declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

trait ResourceWith
{

    /**
     * @param Request $request
     *
     * @return array
     */
    public function with($request): array
    {
        return [
            'meta'     => [],
            'messages' => __('message.success'),
            'status'   => Response::HTTP_OK,
            'success'  => true,
        ];
    }
}
