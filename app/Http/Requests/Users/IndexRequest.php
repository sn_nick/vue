<?php
declare(strict_types=1);

namespace App\Http\Requests\Users;

use App\Traits\RequestFilter;
use Illuminate\Foundation\Http\FormRequest;

class IndexRequest extends FormRequest
{
    use RequestFilter;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [

        ];
    }
}
