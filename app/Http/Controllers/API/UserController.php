<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Components\QueryFilter\Users\UsersIndexFilter;
use App\Components\QueryFilter\Users\UsersShowFilter;
use App\Components\Repository\UserRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Users\{IndexRequest, ShowRequest};
use App\Http\Resources\Users\{UsersResourceCollection, UsersResource};

class UserController extends Controller
{
    private UserRepository $repository;

    /**
     * UserController constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return UsersResourceCollection
     */
    public function index(IndexRequest $request): UsersResourceCollection
    {
        $filters = new UsersIndexFilter($request->getFilterParams());

        $users = $this->repository->index($filters);

        return new UsersResourceCollection($users);
    }

    /**
     * @param ShowRequest $request
     * @return UsersResource
     */
    public function show(ShowRequest $request): UsersResource
    {
        $filters = new UsersShowFilter($request->getFilterParams());
        $params = $request->getUrlParams();
        $user = $this->repository->show($filters, $params);

        return new UsersResource($user);
    }
}
