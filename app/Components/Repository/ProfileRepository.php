<?php
declare(strict_types=1);

namespace App\Components\Repository;

use App\Components\Repository\Contracts\ShowContract;
use App\Components\Repository\Contracts\UpdateContract;
use App\Exceptions\Custom\CustomException;
use App\Models\Profile;
use App\Models\User;
use App\Components\QueryFilter\QueryFilterContract;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class ProfileRepository extends Repository implements ShowContract, UpdateContract
{
    public function show(QueryFilterContract $filters): User
    {
        $profile = User::where('id', Auth::id())->filter($filters)->first();

        if ($profile === null || $profile->email_verified_at === null) {
            throw new CustomException(Response::HTTP_FORBIDDEN, \__('auth.failed'));
        }

        return $profile;
    }

    public function update(array $params): Profile
    {
        $params['user_id'] = Auth::id();

        $profile = Profile::find($params['user_id']);
        $profile->update($params);

        return $profile;
    }

    public function changePassword(array $params): User
    {
        $user = Auth::user();

        $user->update($params);

        return $user;
    }
}
