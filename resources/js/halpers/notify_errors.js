import {forEach} from "lodash";
import notify_error from "./notify_error";

export default function (obj, errors) {
    forEach(errors, (value, index) => {
        notify_error(obj, 'Error ' + index + ' field: ' + value.shift());
    })
}

