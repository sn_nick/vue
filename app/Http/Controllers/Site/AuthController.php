<?php
declare(strict_types=1);

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Response;
use JsonException;

class AuthController extends Controller
{
    /**
     * @param string $token
     * @return Response
     * @throws JsonException
     */
    public function confirm(string $token): Response
    {
        [$email] = \json_decode(\base64_url_decode($token), true, 512, \JSON_THROW_ON_ERROR);

        /** @var User $user */
        $user = User::whereNull('email_verified_at')->where('email', $email)->firstOrFail();
        $user->update(['email_verified_at' => \now()]);

        return \response()->view('index');
    }
}
