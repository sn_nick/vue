init: init-ci
init-ci: docker-down docker-build docker-up imageportal-init
up: docker-up
down: docker-down
restart: down up

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-build:
	docker-compose build --pull

imageportal-init: imageportal-composer-install imageportal-wait-db imageportal-migrations imageportal-storage-link imageportal-caching imageportal-permissions

imageportal-composer-install:
	docker-compose run --rm imageportal-php-cli composer install

imageportal-permissions:
	docker-compose run --rm imageportal-php-cli chmod 777 storage
	docker-compose run --rm imageportal-php-cli chmod 777 bootstrap/cache
	docker-compose run --rm imageportal-php-cli chmod 777 public/js
	docker-compose run --rm imageportal-php-cli chmod 777 public/css
	docker-compose run --rm imageportal-php-cli chmod 777 public/storage

imageportal-wait-db:
	docker-compose run --rm imageportal-php-cli wait-for-it imageportal-postgres:5432 -t 30

imageportal-migrations:
	docker-compose run --rm imageportal-php-cli php artisan migrate --no-interaction

imageportal-seed:
	docker-compose run --rm imageportal-php-cli php artisan migrate:fresh
	docker-compose run --rm imageportal-php-cli php artisan db:seed

imageportal-storage-link:
	docker-compose run --rm imageportal-php-cli php artisan storage:link

imageportal-lint:
	docker-compose run --rm imageportal-php-cli composer lint
	docker-compose run --rm imageportal-php-cli composer php-cs-fixer fix -- --dry-run --diff

imageportal-cs-fix:
	docker-compose run --rm imageportal-php-cli composer php-cs-fixer fix

imageportal-swagger-generate:
	docker-compose run --rm imageportal-php-cli php artisan l5-swagger:generate

imageportal-caching:
	docker-compose run --rm imageportal-php-cli php artisan route:cache
	docker-compose run --rm imageportal-php-cli php artisan config:cache
	docker-compose run --rm imageportal-php-cli php artisan event:cache

imageportal-cache-clear:
	docker-compose run --rm imageportal-php-cli php artisan route:clear
	docker-compose run --rm imageportal-php-cli php artisan config:clear
	docker-compose run --rm imageportal-php-cli php artisan event:clear
	docker-compose run --rm imageportal-php-cli php artisan cache:clear

route:
	docker-compose run --rm imageportal-php-cli php artisan route:list
