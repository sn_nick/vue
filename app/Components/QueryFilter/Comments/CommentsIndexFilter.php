<?php
declare(strict_types=1);

namespace App\Components\QueryFilter\Comments;

use App\Components\QueryFilter\QueryFilter;
use App\Models\EntityType;
use Illuminate\Database\Eloquent\Builder;

class CommentsIndexFilter extends QueryFilter
{
    public function default(): Builder
    {
        return $this->builder;
    }

    public function entity_id(int $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->where('comments.entity_id', $value);
    }

    public function entity_type_id(int $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->where('comments.entity_type_id', $value);
    }

    public function search(string $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->select('comments.*')->where(
            static function ($query) use ($value) {
                $query->where('comments.name', 'like', "$value%")
                      ->orWhere('comments.description', 'like', "%$value%");
            }
        );
    }

    public function image_slug(string $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->select('comments.*')->join(
            'images',
            static function ($join) {
                $join->on('comments.entity_id', '=', 'images.id')
                     ->where('comments.entity_type_id', '=', EntityType::IMAGE_ID);
            }
        )->where('images.slug', $value);
    }
}