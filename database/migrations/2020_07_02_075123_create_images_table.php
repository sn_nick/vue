<?php
declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('images', static function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug');
            $table->string('img');
            $table->string('old_link')->default('');
            $table->bigInteger('image_orientation_id');
            $table->text('description')->nullable();
            $table->bigInteger('like')->default(0);
            $table->bigInteger('view')->default(0);
            $table->bigInteger('load')->default(0);
            $table->bigInteger('theme_id')->default(0);
            $table->bigInteger('image_category_id');
            $table->bigInteger('site_id')->default(0);
            $table->bigInteger('user_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('images');
    }
}
