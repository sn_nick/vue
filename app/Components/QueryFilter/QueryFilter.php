<?php
declare(strict_types=1);

namespace App\Components\QueryFilter;

use Illuminate\Database\Eloquent\Builder;

abstract class QueryFilter implements QueryFilterContract
{
    /**
     * @var Builder $builder
     */
    protected Builder $builder;
    protected FilterParamsContract $params;
    protected string $table;

    public function __construct(FilterParamsContract $params)
    {
        $this->params = $params;
    }

    abstract protected function default(): Builder;

    public function apply($builder): Builder
    {
        $this->builder = $builder;
        $this->table = $this->builder->getModel()->getTable();

        $this->applyFilters();
        $this->applySort();

        return $this->builder;
    }

    protected function applyFilters(): void
    {
        $filters = $this->params->filter();

        if (empty($filters)) {
            $this->default();
            return;
        }

        foreach ($filters as $filter => $value) {
            $methodName = \str_replace('.', '__', $filter);
            if (\method_exists($this, $methodName)) {
                $this->$methodName($value);
                continue;
            }
            $this->default();
            continue;
        }
    }

    protected function applySort(): void
    {
        $sort = $this->params->sort();

        if (empty($sort)) {
            $this->getSort('', 'id', 'desc');

            return;
        }

        $columns = \explode('|', $sort);

        foreach ($columns as $column) {
            if (empty($column)) {
                continue;
            }
            $order = \substr($column, -1) !== '-' ? self::SORT_ASC : self::SORT_DESC;
            $column = \rtrim($column, '-');
            if (empty($column)) {
                continue;
            }
            $table = \strstr($column, '.', true);
            $this->getSort($table ?: '', $column, $order);
        }
    }

    protected function getSort(string $table, string $column, string $order): Builder
    {
        $methodName = 'sort' . (!empty($table) ?
                \str_replace('.', '_', \strstr($column, '.')) :
                '_' . $column);

        if (\method_exists($this, $methodName)) {
            return $this->$methodName($table, $column, $order);
        }

        return $this->builder->orderBy($column, $order);
    }
}
