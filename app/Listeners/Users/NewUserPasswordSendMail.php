<?php
declare(strict_types=1);

namespace App\Listeners\Users;

use App\Events\Users\UserPasswordForgot;
use App\Mail\Users\NewUserPassword;
use Illuminate\Support\Facades\Mail;

class NewUserPasswordSendMail
{
    /**
     * Handle the event.
     *
     * @param UserPasswordForgot $event
     * @return void
     */
    public function handle(UserPasswordForgot $event): void
    {
        Mail::to($event->user)->send(
            (new NewUserPassword($event->dto))->onQueue(\config('queue.send_mail'))
        );
    }
}
