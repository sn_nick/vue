<?php
declare(strict_types=1);

namespace App\Components\Repository;

use App\Components\Repository\Contracts\{DeleteContract, IndexContract, ShowContract, UpdateContract};
use App\Components\UploadingFile\DTO\ImageFileDTO;
use App\Components\UploadingFile\ImageFile;
use App\Models\Image;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Components\QueryFilter\QueryFilterContract;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class ImageRepository extends Repository implements IndexContract, ShowContract, UpdateContract, DeleteContract
{
    public function index(QueryFilterContract $filters): LengthAwarePaginator
    {
        return Image::tie()->filter($filters)->paginate();
    }

    public function show(QueryFilterContract $filters, array $params = []): Image
    {
        $image = Image::where('slug', $params['image'])->tie()->firstOrFail();
        $image->update(['view' => $image->view + 1]);

        return $image;
    }

    public function update(array $params): Image
    {
        $params['user_id'] = Auth::id();

        $image = Image::findOrFail($params['image']);
        $image->update($params);

        return $image;
    }

    public function delete(array $params): Image
    {
        $image = Image::findOrFail($params['image']);
        $image->delete();

        return $image;
    }

    public function upload(array $params, $image_files): Collection
    {
        $dto = new ImageFileDTO((int)$params['image_category_id'], $image_files);

        return (new ImageFile($dto))->create();
    }
}
