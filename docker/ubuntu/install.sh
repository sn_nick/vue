#!/bin/bash

# создание .env файла
function generateDefaultEnv() {
    if ! [[ -f 'example.env' ]]; then
        whiptail --title 'Создание .env файла' --msgbox 'Файл example.env не найден. Продолжить настройку не возможно.' 8 50
        return 0
    fi

    cp -f example.env .env
    whiptail --title 'Создание .env файла' --msgbox \
        'Файл .env успешно создан.\n\n
Требуется настройка переменных:\n
WWW_PATH - путь к корневой www (директория должна быть доступна для пользователя от которого запускаются контейнеры)\n
SSH_KEYS_PATH - путь к директори с ssh ключами (директория должна быть доступна для пользователя от которого запускаются контейнеры)' 15 70
    return 1
}

# редактирование .env
function editEnv() {
    whiptail --title 'Настройка .env' --msgbox 'На следующем этапе будет попытка сборки docker контейнеров/создание проекта и установка пакетов composer. \n
Перед продолжением требуется донастроить файл .env, сейчас будет запущен файл на редактирование в одном из редакторов.' 15 70
    if (hash nano 2>/dev/null); then
        nano ./.env
        return 1
    elif (hash vi 2>/dev/null); then
        vi ./.env
        return 1
    fi
    whiptail --title 'Настройка .env' --msgbox 'Не удалось открыть файл .env на редактирование. Продолжить настройку не возможно.' 8 60
    return 0
}

# запуск docker-compose
function makeDocker() {
    if ! (hash docker-compose 2>/dev/null); then
        whiptail --title 'Сборка docker контейнеров' --msgbox 'В системе не установлен docker-compose. Продолжить настройку не возможно.' 8 60
        return 0
    fi
    if ! [[ -f 'docker-compose.yml' ]]; then
        whiptail --title 'Сборка docker контейнеров' --msgbox 'Не найден файл docker-compose.yml. Продолжить настройку не возможно.' 8 60
        return 0
    fi

    if ! [[ -f '.env' ]]; then
        whiptail --title 'Сборка docker контейнеров' --msgbox 'Не найден файл .env. Продолжить настройку не возможно.' 8 60
        return 0
    fi

    source ./.env
    if [[ "${WWW_PATH: -1}" != '/' ]]; then
        WWW_PATH="${WWW_PATH}/"
    fi

    if [[ ! -d ${WWW_PATH} ]]; then
        mkdir -p ${WWW_PATH}
    fi

    docker-compose -f ./docker-compose.yml down

    docker volume create --name=frontend-composer
    docker volume create --name=frontend-mysql
    docker volume create --name=frontend-redis

    docker-compose -f ./docker-compose.yml build frontend-nginx frontend-mysql frontend-redis frontend-php frontend-node
    docker-compose -f ./docker-compose.yml up --no-build -d
    whiptail --title 'Сборка docker контейнеров' --msgbox 'Сборка и запуск докер контейнеров прошел успешно.' 8 60
    return 1
}

# создание/обновление конфигов
function updateSiteConfigs() {
    if ! [[ -f '.env' ]]; then
        whiptail --title 'Сборка docker контейнеров' --msgbox 'Не найден файл .env. Продолжить настройку не возможно.' 8 60
        return 0
    fi

    source ./.env
    if [[ "${WWW_PATH: -1}" != '/' ]]; then
        WWW_PATH="${WWW_PATH}/"
    fi

    if [[ ! -d ${WWW_PATH} ]]; then
        whiptail --title 'Настройка frontend' --msgbox "Указанная директория \"${WWW_PATH}\" не существует" 8 50
        return 0
    fi

    cp -f "${WWW_PATH}.env.example-docker" "${WWW_PATH}.env"
    cp -f "${WWW_PATH}.env.testing.example-docker" "${WWW_PATH}.env.testing"

    return 1
}

# сборка frontend
function makeSite() {
    updateSiteConfigs

    #Сборка проекта
    docker-compose exec frontend-php /bin/bash -c 'composer install'
    docker-compose exec frontend-php /bin/bash -c 'php artisan migrate:fresh --env=local'
    docker-compose exec frontend-php /bin/bash -c 'php artisan db:seed'
    docker-compose exec frontend-php /bin/bash -c 'php artisan jwt:secret --force'

    #Делаем ссылку
    docker-compose exec frontend-php /bin/bash -c 'rm public/storage'
    docker-compose exec frontend-php /bin/bash -c 'php artisan storage:link'

    #Запускаем тесты
    docker-compose exec frontend-php /bin/bash -c './vendor/bin/phpunit --configuration /var/www/phpunit.xml'

    #Просмотр статуса супервизора
    docker-compose exec frontend-php /bin/bash -c 'supervisorctl status'

    #Даем доступ к файлам проекта для локального пользователя в группе docker
    echo ${LOCAL_USER_PASSWORD} | sudo -S chown -R ${LOCAL_USER}:${LOCAL_GROUP} ${WWW_PATH}/

    #Даем доступ к файлам проекта для группы docker
    echo ${LOCAL_USER_PASSWORD} | sudo -S chmod g+w ${WWW_PATH}/

    #Чистим зависимости проекта
    docker-compose exec frontend-php /bin/bash -c 'php artisan queue:clear'
    docker-compose exec frontend-php /bin/bash -c 'php artisan event:clear'
    docker-compose exec frontend-php /bin/bash -c 'php artisan route:clear'
    docker-compose exec frontend-php /bin/bash -c 'php artisan config:clear'
    docker-compose exec frontend-php /bin/bash -c 'php artisan view:clear'
    docker-compose exec frontend-php /bin/bash -c 'php artisan optimize:clear'
    docker-compose exec frontend-php /bin/bash -c 'php artisan cache:clear'

#    docker-compose -f ./docker-compose.yml down

    return 1
}

# показ меню
function showMenu() {
    case $(whiptail --title "Настройка docker проекта" --menu "Выберите одно из действий:" 15 80 4 \
        "GEN_ENV_DEFAULT" "Сгенерировать docker .env файл по умолчанию" \
        "FULL_INSTALL" "Запустить полную настройку проекта" \
        "UPDATE_INSTALL" "Запустить обновление конфигов проекта" \
        "EXIT" "Выход" 3>&1 1>&2 2>&3) in
    "GEN_ENV_DEFAULT")
        generateDefaultEnv
        showMenu
        ;;
    "FULL_INSTALL")
        if generateDefaultEnv; then
            showMenu
            return
        fi
        if editEnv; then
            showMenu
            return
        fi
        if makeDocker; then
            showMenu
            return
        fi
        if makeSite; then
            showMenu
            return
        fi
        whiptail --title 'Настройка docker проекта' --msgbox 'Настройка успешно завершена' 8 50
        showMenu
        ;;
    "UPDATE_INSTALL")
        if updateSiteConfigs; then
            showMenu
            return
        fi
        whiptail --title 'Настройка docker проекта' --msgbox 'Обновление конфигов успешно завершено' 8 50
        showMenu
        ;;
    *)
        exit 1
        ;;
    esac
}

showMenu
