<?php
declare(strict_types=1);

namespace App\Components\QueryFilter\Profiles;

use App\Components\QueryFilter\QueryFilter;
use Illuminate\Database\Eloquent\Builder;

class ProfileShowFilter extends QueryFilter
{
    public function default(): Builder
    {
        return $this->builder;
    }

    public function articles($values): Builder
    {
        if (empty($values)) {
            return $this->builder;
        }

        return $this->builder->with([
            'articles' => function ($query) use ($values) {
                $builder = $query->with(['articleCategory', 'user.profile']);

                if (!empty($values['article_type_id'])) {
                    $builder->where('articles.article_type_id', $values['article_type_id']);
                }

                if (!empty($values['article_category_id'])) {
                    $builder->where('articles.article_category_id', $values['article_category_id']);
                }

                if (!empty($values['search'])) {
                    $builder
                        ->where('articles.name', 'like', "%{$values['search']}%")
                        ->orWhere('articles.description', 'like', "%{$values['search']}%");
                }
                $builder->orderBy('id', 'desc');
                $builder->paginate();
            }
        ]);
    }

    public function images($values): Builder
    {
        return $this->builder->with([
            'images' => function ($query) use ($values) {
                $builder = $query->with(['imageCategory', 'user.profile']);

                if (!empty($values['image_category_id'])) {
                    $builder->where('images.image_category_id', $values['image_category_id']);
                }

                $builder->orderBy('id', 'desc');
                $builder->paginate();
            }
        ]);
    }
}
