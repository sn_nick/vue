<?php
declare(strict_types=1);

namespace App\Http\Resources\Auth;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    use ResourceWith;

    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        if (empty($this->resource)) {
            return [];
        }

        $this->load(['profile', 'articles.articleCategory', 'articles.user.profile', 'images', 'comments']);

        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'profile' => $this->profile,
            'articles' => $this->articles,
            'images' => $this->images,
            'comments' => $this->comments,
        ];
    }
}
