<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EntityType extends Model
{
    public const ARTICLE_ID = 1;
    public const IMAGE_ID = 2;
    public const COMMENT_ID = 3;

    protected $fillable = [
        'name'
    ];

    public $timestamps = false;
}
