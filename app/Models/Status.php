<?php
declare(strict_types=1);

namespace App\Models;

use Spatie\ModelStatus\Status as SpatieStatus;

class Status extends SpatieStatus
{
    public const PENDING_STATUS = 'pending';
    public const ACCEPTED_STATUS = 'accepted';
    public const REJECTED_STATUS = 'rejected';
}
