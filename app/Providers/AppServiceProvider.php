<?php

namespace App\Providers;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Image;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->morphMapRelations();
//        \DB::listen(function($query){
//            dump($query->sql);
//        });
    }

    private function morphMapRelations()
    {
        Relation::morphMap([
            'image' => Image::class,
            'comment' => Comment::class,
            'article' => Article::class,
        ]);
    }
}
