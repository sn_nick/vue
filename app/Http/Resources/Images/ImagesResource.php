<?php
declare(strict_types=1);

namespace App\Http\Resources\Images;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ImagesResource extends JsonResource
{
    use ResourceWith;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        if (empty($this->resource)) {
            return [];
        }

        /** @var \App\Models\Image $this */

        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'img' => $this->img,
            'image_src' => $this->image_src,
            'image_orientation_id' => $this->image_orientation_id,
            'description' => $this->description,
            'like' => $this->like,
            'view' => $this->view,
            'load' => $this->load,
            'created_at' => $this->created_at,
            'user' => $this->user,
            'site' => $this->site,
            'image_category' => $this->imageCategory,
            'synthetic' => $this->synthetic,
        ];
    }
}
