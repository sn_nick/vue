#!/bin/bash

dockerFiles='-f ./docker-compose.yml'
buildContainers='frontend-nginx frontend-mysql frontend-redis'
dockerToggle='up --no-build -d'

listArgs=("$@")


#########################
# The command line help #
#########################
function display_help() {
    echo "Usage: $0 [option...] {up|down|restart}" >&2
    echo
    echo "Параметры:"
    echo "   up                Поднять контейнеры"
    echo "   down              Потушить контейнеры"
    echo "   restart           Перезапустить контейнеры"
    echo
    echo "Examples:"
    echo "  ./docker.sh up и ./docker.sh down и ./docker.sh restart"
    echo
    exit 0
}

for opt in "${listArgs[@]}"; do
  case $opt in
  -h | --help)
    display_help
    ;;
  down)
    dockerToggle='down'
    ;;
  restart)
    dockerToggle='restart'
    ;;
  esac
done

if [ "$dockerToggle" = "restart" ]; then
  docker-compose $dockerFiles down
  dockerToggle='up --no-build -d'
fi

if [ "$dockerToggle" != "down" ]; then
  docker-compose $dockerFiles build $buildContainers
  docker-compose $dockerFiles build frontend-php
  docker-compose $dockerFiles build frontend-node
fi

docker-compose $dockerFiles $dockerToggle
