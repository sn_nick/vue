<?php
declare(strict_types=1);

namespace App\Components\UploadingFile;

use App\Components\UploadingFile\DTO\ImageFileDTO;
use App\Models\Image;
use App\Models\ImageOrientation;
use Illuminate\Http\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use InterventionImage;
use Intervention\Image\Image as Img;

final class ImageFileUploading
{
    protected Image $model;
    protected ImageFileDTO $dto;

    public function __construct(Image $model, ImageFileDTO $dto)
    {
        $this->model = $model;
        $this->dto = $dto;
    }

    public function upload(UploadedFile $uploadedFile): File
    {
        $filename = get_random_filename();

        $file = $uploadedFile->move($this->getFilePath($filename), $filename);

        $this->initImageOrientationId($file);

        $this->dto->addImage($uploadedFile, $file);

        return $file;
    }

    public function getData(): array
    {
        return $this->dto->getImageCollection()->toArray();
    }

    protected function getFilePath(string $filename): string
    {
        return $this->model->getStorageDirectoryFilePathForUpload($filename);
    }

    private function initImageOrientationId($file): void
    {
        $img = InterventionImage::make($file->getRealPath());

        $this->saveHorizontal($img);
        $this->saveVertical($img);
    }

    private function saveHorizontal(Img $img): void
    {
        if($img->width() > $img->height()){
            $img->fit(800, 533);
            $img->save();

            $this->dto->setImageOrientationId(ImageOrientation::HORIZONTAL_ID);
        }
    }

    private function saveVertical(Img $img): void
    {
        if($img->width() < $img->height()){
            $img->fit(533, 800);
            $img->save();

            $this->dto->setImageOrientationId(ImageOrientation::VERTICAL_ID);
        }
    }
}
