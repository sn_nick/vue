<?php
declare(strict_types=1);

namespace App\Components\Repository\Contracts;

use Illuminate\Database\Eloquent\Model;

interface UpdateContract
{
    public function update(array $params): Model;
}