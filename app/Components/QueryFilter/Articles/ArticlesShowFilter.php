<?php
declare(strict_types=1);

namespace App\Components\QueryFilter\Articles;

use App\Components\QueryFilter\QueryFilter;
use App\Models\ArticleType;
use App\Models\Status;
use Illuminate\Database\Eloquent\Builder;

class ArticlesShowFilter extends QueryFilter
{
    public function apply($builder): Builder
    {
        return parent::apply($builder)->currentStatus(Status::ACCEPTED_STATUS);
    }

    public function default(): Builder
    {
        return $this->builder->where('article_type_id', ArticleType::FORUM_ID);
    }
}