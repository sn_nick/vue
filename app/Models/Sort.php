<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Sort extends Model
{
    protected $fillable = [
        'name', 'value', 'entity_type_id'
    ];

    public $timestamps = false;

    public function images(): HasMany
    {
        return $this->hasMany(Image::class)->where('entity_type_id', '=', EntityType::IMAGE_ID);
    }
}
