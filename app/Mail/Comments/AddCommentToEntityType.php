<?php
declare(strict_types=1);

namespace App\Mail\Comments;

use App\Models\Article;
use App\Models\Image;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Throwable;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AddCommentToEntityType extends Mailable
{
    use Queueable;
    use SerializesModels;

    /** @var Image|Article $entity */
    public $entity;

    public function __construct(Model $entity)
    {
        $this->entity = $entity;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): self
    {
        return $this
            ->from(\config('mail.from.address'))
            ->view('mail.comments.add-comment-to-entity-type')
            ->subject('Add comment');
    }

    /**
     * The job failed to process.
     *
     * @param Throwable $exception
     * @return void
     */
    public function failed(Throwable $exception): void
    {
        Log::error('Error send mail add comment to article', [$exception]);
    }
}
