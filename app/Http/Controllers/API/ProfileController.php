<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Components\QueryFilter\Profiles\ProfileShowFilter;
use App\Components\Repository\ProfileRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Profiles\ChangePasswordRequest;
use App\Http\Requests\Profiles\ShowRequest;
use App\Http\Requests\Profiles\UpdateRequest;
use App\Http\Resources\Profiles\ProfilesResource;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Auth\{UserResource};

class ProfileController extends Controller
{
    private ProfileRepository $repository;

    /**
     * ProfileController constructor.
     * @param ProfileRepository $repository
     */
    public function __construct(ProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param ShowRequest $request
     * @return ProfilesResource
     */
    public function show(ShowRequest $request): ProfilesResource
    {
        $filters = new ProfileShowFilter($request->getFilterParams());
        $profile = $this->repository->show($filters);

        return new ProfilesResource($profile);
    }

    /**
     * @param UpdateRequest $request
     * @return UserResource
     */
    public function update(UpdateRequest $request): UserResource
    {
        $this->repository->update($request->getRequestParams());

        return new UserResource([]);
    }

    /**
     * @param ChangePasswordRequest $request
     * @return UserResource
     */
    public function changePassword(ChangePasswordRequest $request): UserResource
    {
        $this->repository->changePassword($request->getRequestParams());

        return new UserResource([]);
    }
}
