export default {
    article_types: {
        blog: 1,
        forum: 2,
    },
    entity_types: {
        article: 1,
        image: 2,
        comment: 3,
        article_type: 'article',
        image_type: 'image',
        comment_type: 'comment',
    }
}