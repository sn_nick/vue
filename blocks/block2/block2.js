"use strict";

function toggleSearchMenu(elem) {
    let searchMenu = elem.parentNode.children[3];
    searchMenu.style.display = searchMenu.style.display === 'none' ? 'block' : 'none';
}