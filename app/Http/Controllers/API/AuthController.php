<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Components\DTO\Users\UserRegisteredDTO;
use App\Events\Users\UserPasswordForgot;
use App\Events\Users\UserRegistered;
use App\Exceptions\Custom\CustomException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use App\Http\Requests\Auth\RegisterFormRequest;
use App\Http\Resources\Auth\{ForgotPasswordResource,
    LoginResource,
    LogoutResource,
    RefreshResource,
    RegisterResource,
    UserResource};
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    private UserRegisteredDTO $dto;

    /**
     * AuthController constructor.
     * @param UserRegisteredDTO $dto
     */
    public function __construct(UserRegisteredDTO $dto)
    {
        $this->dto = $dto;
    }

    public function register(RegisterFormRequest $request): RegisterResource
    {
        $this->dto->setEmail($request->get('email'));
        $this->dto->setPassword(Str::random(8));

        $params = \json_encode([$this->dto->getEmail(), $this->dto->getPassword()], \JSON_THROW_ON_ERROR);
        $this->dto->setToken(\base64_url_encode($params));

        $data = [
            'email'    => $this->dto->getEmail(),
            'name'     => $this->dto->getEmail(),
            'password' => $this->dto->getPassword(),
        ];

        $user = DB::transaction(function () use ($data) {
            $user = User::create($data);
            $user->profile()->create(['user_id' => $user->id]);

            return $user;
        });

        event(new UserRegistered($user, $this->dto));

        return new RegisterResource($user);
    }

    public function login(Request $request): JsonResponse
    {
        $credentials = $request->only('email', 'password');

        if (!$token = JWTAuth::attempt($credentials)) {
            throw new CustomException(Response::HTTP_FORBIDDEN, \__('auth.failed'));
        }

        $auth = Auth::user();
        if ($auth === null || $auth->email_verified_at === null) {
            throw new CustomException(Response::HTTP_FORBIDDEN, \__('auth.failed'));
        }

        $resource = new LoginResource($auth);

        return $resource->response()->header('Authorization', $token);
    }

    public function logout(): LogoutResource
    {
        JWTAuth::invalidate();

        return new LogoutResource([]);
    }

    public function user(): UserResource
    {
        $auth = Auth::user();
        if ($auth === null || $auth->email_verified_at === null) {
            throw new CustomException(Response::HTTP_FORBIDDEN, \__('auth.failed'));
        }

        return new UserResource($auth);
    }

    public function forgotPassword(ForgotPasswordRequest $request): ForgotPasswordResource
    {
        $this->dto->setEmail($request->get('email'));
        $this->dto->setPassword(Str::random(8));

        $user = User::where('email', $this->dto->getEmail())->first();
        $user->update(['password' => $this->dto->getPassword()]);

        event(new UserPasswordForgot($user, $this->dto));

        return new ForgotPasswordResource($user);
    }

    public function refresh(): RefreshResource
    {
        return new RefreshResource([]);
    }
}
