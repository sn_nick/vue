/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import Vue from "vue";

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

import router from "./router/router";
import store from "./vuex/store";
import App from "./components/App";
import bootstrap from './bootstrap/bootstrap';
import vuetify from './vuetify/vuetify';
import loader from './infinite-loading/loader';
import dateFormat from './dateformat/dateformat';
import CKEditor from './ckeditor/ckeditor';
import datePicker from './datetimepicker/datetimepicker';
import Notifications from './notifications/notifications';

/**
 * Компонент аутентификации пользователя
 */
import auth            from '@websanova/vue-auth';
import authBasic       from '@websanova/vue-auth/dist/drivers/auth/basic.esm.js';
import authBearer       from '@websanova/vue-auth/dist/drivers/auth/bearer.esm.js';
import httpVueResource from '@websanova/vue-auth/dist/drivers/http/vue-resource.1.x.esm.js';
import routerVueRouter from '@websanova/vue-auth/dist/drivers/router/vue-router.2.x.esm.js';
import oauth2Google    from '@websanova/vue-auth/dist/drivers/oauth2/google.esm.js';
import oauth2Facebook  from '@websanova/vue-auth/dist/drivers/oauth2/facebook.esm.js';

oauth2Google.params.client_id = 'GOOGLE-OAUTH2-CLIENT-ID';
oauth2Facebook.params.client_id = 'FACEBOOK-CLIENT-ID';
Vue.router = router
Vue.use(auth, {
    auth: authBearer,
    http: httpVueResource,
    router: routerVueRouter,
    oauth2: {
        google: oauth2Google,
        facebook: oauth2Facebook,
    },
    rolesKey: 'role',

    //loginData: {url: '/api/auth/login', method: 'POST', redirect: '/dashboard', fetchUser: true},
    fetchData: {url: '/api/auth/user', method: 'GET', enabled: true},
    //refreshData: {url: '/api/auth/user', method: 'GET', enabled: true, interval: 5}, //TODO повторный запрос на получение юзера с интервалом.
    parseUserData: function(res) { return res; },
    logoutData: {url: '/api/auth/logout', method: 'POST', redirect: '/login', makeRequest: false},
});

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#App',
    render : h => h(App),
    router,
    store,
    bootstrap,
    vuetify,
    loader,
    dateFormat,
    CKEditor,
    datePicker,
    Notifications
});
