<?php
declare(strict_types=1);

namespace App\Listeners\Articles;

use App\Events\Articles\ArticleCreated;

class ArticleSlugUpdate
{
    /**
     * Handle the event.
     *
     * @param ArticleCreated $event
     * @return void
     */
    public function handle(ArticleCreated $event): void
    {
        $event->article->update(['slug' => "{$event->article->id}-{$event->article->slug}"]);
    }
}
