<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Components\QueryFilter\Images\{ImagesIndexFilter, ImagesShowFilter};
use App\Components\Repository\ImageRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Images\{DeleteRequest, IndexRequest, ShowRequest, StoreRequest, UpdateRequest};
use App\Http\Resources\Images\{ImagesResource, ImagesResourceCollection};
use App\Models\Image;
use InterventionImage;

class ImageController extends Controller
{
    private ImageRepository $repository;

    /**
     * ImageController constructor.
     * @param ImageRepository $repository
     */
    public function __construct(ImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return ImagesResourceCollection
     */
    public function index(IndexRequest $request): ImagesResourceCollection
    {
        $filters = new ImagesIndexFilter($request->getFilterParams());
        $images = $this->repository->index($filters);

        return new ImagesResourceCollection($images);
    }

    /**
     * @param ShowRequest $request
     * @return ImagesResource
     */
    public function show(ShowRequest $request): ImagesResource
    {
        $filters = new ImagesShowFilter($request->getFilterParams());
        $params = $request->getUrlParams();
        $image = $this->repository->show($filters, $params);

        $resource = new ImagesResource($image);
        $resource->additional([
            'meta' => ['images' => Image::similar($params['image'])->get()],
        ]);

        return $resource;
    }

    /**
     * @param UpdateRequest $request
     * @return ImagesResource
     */
    public function update(UpdateRequest $request): ImagesResource
    {
        $image = $this->repository->update($request->getAllRequestParams());

        return new ImagesResource($image);
    }

    /**
     * @param DeleteRequest $request
     * @return ImagesResource
     */
    public function destroy(DeleteRequest $request): ImagesResource
    {
        $image = $this->repository->delete($request->getUrlParams());

        return new ImagesResource($image);
    }

    /**
     * @param StoreRequest $request
     * @return ImagesResourceCollection
     */
    public function upload(StoreRequest $request): ImagesResourceCollection
    {
        $image_files = $request->getRequestFiles();
        $params = $request->getRequestParams();

        $images = $this->repository->upload($params, $image_files);

        return new ImagesResourceCollection($images);
    }
}
