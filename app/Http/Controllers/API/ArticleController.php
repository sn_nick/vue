<?php
declare(strict_types=1);

namespace App\Http\Controllers\API;

use App\Components\QueryFilter\Articles\ArticlesIndexFilter;
use App\Components\QueryFilter\Articles\ArticlesShowFilter;
use App\Components\Repository\ArticleRepository;
use App\Http\Controllers\Controller;
use App\Http\Requests\Articles\IndexRequest;
use App\Http\Requests\Articles\ShowRequest;
use App\Http\Requests\Articles\StoreRequest;
use App\Http\Resources\Articles\ArticlesResource;
use App\Http\Resources\Articles\ArticlesResourceCollection;
use App\Models\{ArticleCategory};

class ArticleController extends Controller
{
    private ArticleRepository $repository;

    /**
     * ArticleController constructor.
     * @param ArticleRepository $repository
     */
    public function __construct(ArticleRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return ArticlesResourceCollection
     */
    public function index(IndexRequest $request): ArticlesResourceCollection
    {
        $filters = new ArticlesIndexFilter($request->getFilterParams());
        $articles = $this->repository->index($filters);

        return new ArticlesResourceCollection($articles);
    }

    /**
     * @param StoreRequest $request
     * @return ArticlesResource
     */
    public function store(StoreRequest $request): ArticlesResource
    {
        $params = $request->getRequestParams();
        $article = $this->repository->store($params);

        return new ArticlesResource($article);
    }

    /**
     * @param ShowRequest $request
     * @return ArticlesResource
     */
    public function show(ShowRequest $request): ArticlesResource
    {
        $filters = new ArticlesShowFilter($request->getFilterParams());
        $params = $request->getUrlParams();
        $article = $this->repository->show($filters, $params);

        return new ArticlesResource($article);
    }
}
