<?php
declare(strict_types=1);

namespace App\Components\UploadingFile\Contracts;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\File\File;

interface FileDTOContract
{
    public function addImage(UploadedFile $uploaded_file, File $file): void;

    public function getImageCollection(): Collection;
}
