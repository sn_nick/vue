<?php
declare(strict_types=1);

namespace App\Components\Repository\Contracts;

use App\Components\QueryFilter\QueryFilterContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

interface IndexContract
{
    public function index(QueryFilterContract $filters): LengthAwarePaginator;
}