import Vue from 'vue';
import datePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';

import 'vue2-datepicker/locale/ru';
Vue.use(datePicker);

export default datePicker;
