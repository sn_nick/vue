<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Активация регистрации нового ползователя (New user registration activation)</title>
    </head>
    <body>
        <h1>Спасибо за регистрацию!</h1>
        <p>Ваш пароль: {{$password}}</p>
        <p>Перейдите <a href='{{ route('confirm', ['token' => $token]) }}'>по ссылке </a>чтобы завершить регистрацию!</p>
    </body>
</html>
