<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Добавлена новая статья в разделе форум (Added a new article in the forum section)</title>
</head>
<body>
<h1>Respected, {{$user->profile->full_name ?: 'Client'}}!</h1>
<p>Added a new article in the forum section</p>
<p>Go to article <a href="{{config('app.url')}}/article/{{$article->slug}}">{{$article->name}}</a></p>


<h1>Уважаемый, {{$user->profile->full_name ?: 'Клиент'}}!</h1>
<p>Добавлена новая статья в разделе форум</p>
<p>Перейти к статье <a href="{{config('app.url')}}/article/{{$article->slug}}">{{$article->name}}</a></p>
</body>
</html>
