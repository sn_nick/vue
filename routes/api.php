<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['namespace' => 'API'], static function () {
    Route::post('auth/login', [
        'as' => 'auth.login',
        'uses' => 'AuthController@login'
    ]);
    Route::post('auth/register', [
        'as' => 'auth.register',
        'uses' => 'AuthController@register'
    ]);
    Route::post('auth/logout', [
        'as' => 'auth.logout',
        'uses' => 'AuthController@logout'
    ]);
    Route::post('auth/forgot-password', [
        'as' => 'auth.forgotPassword',
        'uses' => 'AuthController@forgotPassword'
    ]);
    Route::group(['middleware' => 'jwt.auth'], static function () {
        Route::get('auth/user', [
            'as' => 'auth.user',
            'uses' => 'AuthController@user'
        ]);
        Route::post('logout', [
            'as' => 'logout',
            'uses' => 'AuthController@logout'
        ]);
        Route::post('profiles.update', [
            'as' => 'profiles.update',
            'uses' => 'ProfileController@update'
        ]);
        Route::get('profiles.show', [
            'as' => 'profiles.show',
            'uses' => 'ProfileController@show'
        ]);
        Route::delete('profiles.destroy', [
            'as' => 'profiles.destroy',
            'uses' => 'ProfileController@destroy'
        ]);
        Route::put('profiles.changePassword', [
            'as' => 'profiles.changePassword',
            'uses' => 'ProfileController@changePassword'
        ]);
    });

    Route::group(['middleware' => 'jwt.refresh'], static function () {
        Route::get('refresh', 'AuthController@refresh');
    });


    Route::post('images.upload', [
        'as' => 'images.upload',
        'uses' => 'ImageController@upload'
    ]);
    Route::apiResource('images', 'ImageController');
    Route::apiResource('articles', 'ArticleController');
    Route::apiResource('comments', 'CommentController');

    Route::get('dictionaries', [
        'as' => 'dictionaries',
        'uses' => 'DictionaryController'
    ]);
});