import {mapActions} from "vuex";

export default {
    methods: {
        ...mapActions([
            'GET_DICTIONARIES',
            'ADD_ARTICLES_DASHBOARD_PAGE',
            'ADD_ARTICLES_DASHBOARD_FILTERS'
        ]),
        search_filter(search) {
            let filters = {search: search}
            this.filters(filters)
        },

        article_category_filter(article_category_id) {
            let filters = {article_category_id: article_category_id}
            this.filters(filters)
        },
        filters(filters) {
            this.ADD_ARTICLES_DASHBOARD_PAGE(1)
            this.ADD_ARTICLES_DASHBOARD_FILTERS(filters)
            this.$store.state.articles.dashboard.articles = [];
        },
    }
}