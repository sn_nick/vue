import Vue from 'vue';
import Vuex from 'vuex';
import apiActions from "./actions/api-actions";
import appActions from "./actions/app-actions";
import mutations from "./mutations/mutations";
import getters from "./getters/getters";

const actions = {...apiActions, ...appActions};

Vue.use(Vuex);

let store = new Vuex.Store({
    state: {
        images: {
            dashboard: {
                images: [],
                url_params: {
                    page: 1,
                    filters: {},
                    sort: '',
                },
            },
            home: [],
            catalog: [],
            similar: [],
            url_params: {
                page: 1,
                filters: {},
                sort: '',
            },
            image: {},
        },

        articles: {
            dashboard: {
                articles: [],
                comments: [],
                url_params: {
                    page: 1,
                    filters: {},
                    sort: '',
                },
            },
            catalog: [],
            forum: [],
            blog: [],
            url_params: {
                page: 1,
                blog_page: 1,
                forum_page: 1,
                filters: {},
                sort: '',
            },
            article: {},
        },

        comments: {
            catalog: [],
            url_params: {
                page: 1,
                filters: {},
                sort: '',
            },
            comment: {},
        },

        dictionaries: [],
        handler: false,
    },
    mutations,
    actions,
    getters,
});

export default store
