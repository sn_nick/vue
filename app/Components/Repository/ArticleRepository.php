<?php
declare(strict_types=1);

namespace App\Components\Repository;

use App\Components\Repository\Contracts\IndexContract;
use App\Components\Repository\Contracts\ShowContract;
use App\Components\Repository\Contracts\StoreContract;
use App\Models\Article;
use App\Models\ArticleType;
use App\Models\Status;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use App\Components\QueryFilter\QueryFilterContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ArticleRepository extends Repository implements IndexContract, StoreContract, ShowContract
{
    public function index(QueryFilterContract $filters): LengthAwarePaginator
    {
        return Article::with('user.profile', 'articleCategory')
                      ->filter($filters)
                      ->paginate();
    }

    public function store(array $params): Article
    {
        $params['user_id'] = Auth::id();
        $params['slug'] = Str::slug($params['name'], '-');

        $article = Article::create($params);
        $article->setStatus(Status::ACCEPTED_STATUS);
        $article->refresh();

        return $article;
    }

    public function show(QueryFilterContract $filters, array $params = []): Article
    {
        $article = Article::with([
            'user.profile',
            'articleCategory',
            'comments' => static function ($query) {
                return $query->with('user');
            },
        ])
        ->where('slug', (int)$params['article'])
        ->filter($filters)
        ->firstOrFail();

        $article->update(['view' => $article->view + 1]);

        return $article;
    }
}
