import {mapActions} from "vuex";
import constants from "../../halpers/constants";
import fi from "vue2-datepicker/locale/es/fi";

export default {
    methods: {
        ...mapActions([
            'GET_DICTIONARIES',
            'ADD_ARTICLES_FORUM_PAGE',
            'ADD_ARTICLES_FORUM_PAGE',
            'ADD_ARTICLES_FILTERS',
        ]),
        search_filter(search) {
            let filters = {search: search}
            this.filters(filters)
        },

        article_category_filter(article_category_id, article_type_id) {
            let filters = {article_category_id: article_category_id, article_type_id: article_type_id}

            this.filters(filters)
        },
        filters(filters) {
            this.ADD_ARTICLES_FORUM_PAGE(1)
            this.ADD_ARTICLES_FILTERS(filters)
            this.$store.state.articles.forum = [];
            this.$store.state.articles.blog = [];

            if(filters.article_type_id === constants.article_types.blog && this.$route.name !== 'blog'){
                this.$router.push('/blog')
            }

            if(filters.article_type_id === constants.article_types.forum && this.$route.name !== 'forum'){
                this.$router.push('/forum')
            }
        },
    }
}