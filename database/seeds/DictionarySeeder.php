<?php
declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DictionarySeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $dir = base_path('database/seeds/dictionaries');

        $files = \scandir($dir);

//        DB::statement(/** @lang MySQL */ "SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO'");

        foreach ($files as $file) {
            $path = \realpath($dir . DIRECTORY_SEPARATOR . $file);

            if (!\is_dir($path)) {
                try {
                    $sql = \file_get_contents($path);
                    DB::unprepared($sql);
                } catch (\Exception $e) {
                    $this->command->error('Exception: ' . $e->getMessage() . '. Line: ' . $e->getLine());
                }
            }
        }
    }
}
