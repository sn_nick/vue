<?php
declare(strict_types=1);

namespace App\Models;

use App\Events\Articles\ArticleCreated;
use App\Traits\ScopeFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Spatie\ModelStatus\HasStatuses;

class Article extends Model
{
    use ScopeFilter;
    use HasStatuses;

    protected $dispatchesEvents = [
        'created' => ArticleCreated::class,
    ];

    protected $fillable = [
        'name',
        'slug',
        'img',
        'description',
        'like',
        'view',
        'user_id',
        'article_type_id',
        'article_category_id',
    ];

    protected $perPage = 30;

    protected $appends = ['synthetic'];

    public function articleType(): BelongsTo
    {
        return $this->belongsTo(ArticleType::class);
    }

    public function articleCategory(): BelongsTo
    {
        return $this->belongsTo(ArticleCategory::class);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class, 'entity_id', 'id')
                    ->where('entity_type_id', '=', EntityType::ARTICLE_ID);
    }

    public function entity_comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'entity');
    }

    public function getSyntheticAttribute(): array
    {
        $this->attributes['synthetic']['count_comments'] = $this->comments()->count();
        $this->attributes['synthetic']['last_comment'] = $this->getLastCommentInArticle() ?: $this->getArticleWithUser();
        $this->attributes['synthetic']['image_src'] = '/img/articles/' . $this->img;

        return $this->attributes['synthetic'];
    }

    public function getLastCommentInArticle()
    {
        return $this
                ->comments()
                ->with('user.profile')
                ->orderBy('id', 'desc')
                ->limit(1)
                ->first() ?? [];
    }

    public function getArticleWithUser()
    {
        return self::with('user.profile')->first();
    }
}
