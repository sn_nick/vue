<?php
declare(strict_types=1);

namespace App\Components\QueryFilter\Articles;

use App\Components\QueryFilter\QueryFilter;
use App\Models\Status;
use Illuminate\Database\Eloquent\Builder;

class ArticlesIndexFilter extends QueryFilter
{
    public function apply($builder): Builder
    {
        return parent::apply($builder)->currentStatus(Status::ACCEPTED_STATUS);
    }

    public function default(): Builder
    {
        return $this->builder;
    }

    public function article_category_id(int $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->where('article_category_id', $value);
    }

    public function article_type_id(int $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->where('article_type_id', $value);
    }

    public function search(string $value): Builder
    {
        if (empty($value)) {
            return $this->builder;
        }

        return $this->builder->select('articles.*')->where(
            static function ($query) use ($value) {
                $query->where('name', 'like', "$value%")
                      ->orWhere('description', 'like', "%$value%");
            }
        );
    }
}