<?php
declare(strict_types=1);

namespace App\Helpers;

final class Time
{
    public const MINUTE = 60;
    public const HOUR = 60 * self::MINUTE;
    public const DAY = 24 * self::HOUR;
    public const WEEK = 7 * self::DAY;
    public const MONTH = 30 * self::DAY;
    public const YEAR = 365 * self::DAY;
}
