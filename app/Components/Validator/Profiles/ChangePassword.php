<?php
declare(strict_types=1);

namespace App\Components\Validator\Profiles;

use App\Components\Validator\StrategyContract;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class ChangePassword implements StrategyContract
{
    public function afterValidate(Request $request, Validator $validator): void
    {
        $validator->after(function (Validator $validator) use ($request) {
            $user = Auth::user();
            $oldPassword = $request->get('old_password');
            $password = $request->get('password');

            //TODO переделать ошибки, когда будут переводы
            if (!Hash::check($oldPassword, $user->password)) {
                $validator->errors()->add('old_password', 'The current password does not match the specified password.');
            }

            if (!strcmp($oldPassword, $password)) {
                $validator->errors()->add('password', 'The new password cannot be the same as the current password.');
            }
        });
    }
}