<?php
declare(strict_types=1);

namespace App\Components\Repository\Contracts;

use App\Components\QueryFilter\QueryFilterContract;
use Illuminate\Database\Eloquent\Model;

interface ShowContract
{
    public function show(QueryFilterContract $filters): Model;
}