import {mapActions} from "vuex";

export default {
    methods: {
        ...mapActions([
            'ADD_COMMENTS_PAGE',
            'ADD_COMMENTS_FILTERS',
        ]),
        search_comments_filter(search){
            let filters = {search: search}
            this.comments_filters(filters)
        },
        comments_filters(filters) {
            this.ADD_COMMENTS_PAGE(1)
            this.ADD_COMMENTS_FILTERS(filters)
            this.$store.state.comments.catalog = [];
        },
    }
}