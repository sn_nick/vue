INSERT INTO comments
VALUES (1, 'Тестовый комментарий для сущности article с id=1 в форуме пользователь 1', 'Commercial use images for mockups...', 1, 'article', 1, 1, '2020-11-03 00:00:00', '2020-11-03 00:00:00', NULL),
       (2, 'Комментарий для сущности article с id=1 в форуме пользователь 1', 'About the licence notification on each image/video...', 1, 'article', 1, 1, '2020-11-04 00:00:00', '2020-11-04 00:00:00', NULL),
       (3, 'Комментарий тестовый для сущности article с id=3 в форуме пользователь 1', 'Can I use Pixabay Images in PPT Templates...', 1, 'article', 1, 3, '2020-11-05 00:00:00', '2020-11-05 00:00:00', NULL);
SELECT setval('comments_id_seq', (SELECT MAX(id) FROM comments)+1);
