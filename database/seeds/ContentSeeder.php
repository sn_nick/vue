<?php
declare(strict_types=1);

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\{Image, Article, Comment, Status};

class ContentSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        $dir = base_path('database/seeds/contents');

        $files = \scandir($dir);

//        DB::statement(/** @lang MySQL */ "SET SESSION sql_mode='NO_AUTO_VALUE_ON_ZERO'");

        foreach ($files as $key => $file) {
            $path = \realpath($dir . DIRECTORY_SEPARATOR . $file);
            if (!\is_dir($path)) {
                try {
                    $sql = \file_get_contents($path);
                    $pattern = '/(\()(\d+)(,.+)/i';

                    preg_match_all($pattern, $sql, $matches);
                    $ids = $matches[2];

                    DB::unprepared($sql);

                    foreach ($ids as $id) {
                        if ($key === 2) {
                            $model = Image::find((int)$id);
                        }
                        if ($key === 3) {
                            $model = Article::find((int)$id);
                        }
                        if ($key === 4) {
                            $model = Comment::find((int)$id);
                        }

                        if ($model !== null) {
                            $model->setStatus(Status::ACCEPTED_STATUS);
                        }
                    }
                } catch (\Exception $e) {
                    $this->command->error('Exception: ' . $e->getMessage() . '. Line: ' . $e->getLine());
                }
            }
        }
    }
}
