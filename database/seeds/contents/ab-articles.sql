INSERT INTO articles
VALUES (1, 'Тестовая статья для сущности форум user_id=1 два комментарий', '1', '1', 'Commercial use images for mockups', 1, 1, 1, 2, 1, '2020-11-02 00:00:00', '2020-11-02 00:00:00', NULL),
       (2, 'Статья тестовай для сущности форум user_id=1 без комментариев', '2', '2','About the licence notification on each image/video', 2, 2, 1, 2, 2, NOW(), NOW(), NULL),
       (3, 'Статья для сущности форум user_id=1 один комментария', '3', '3', 'Can I use Pixabay Images in PPT Templates?', 3, 3, 1, 2, 3, '2020-10-21 00:00:00', '2020-10-21 00:00:00', NULL),
       (4, 'Блог тестовая статья', '4', '4','image/video', 2, 2, 1, 1, 1, NOW(), NOW(), NULL),
       (5, 'Блог тестовая статья', '5', '5','image/video', 2, 2, 1, 1, 2, NOW(), NOW(), NULL),
       (6, 'Блог тестовая статья', '6', '6','image/video', 2, 2, 1, 1, 3, NOW(), NOW(), NULL);
SELECT setval('articles_id_seq', (SELECT MAX(id) FROM articles)+1);
