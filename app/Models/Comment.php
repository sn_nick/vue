<?php
declare(strict_types=1);

namespace App\Models;

use App\Events\Comments\CommentCreated;
use App\Traits\ScopeFilter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Spatie\ModelStatus\HasStatuses;

class Comment extends Model
{
    use ScopeFilter;
    use HasStatuses;

    protected $dispatchesEvents = [
        'created' => CommentCreated::class,
    ];

    protected $fillable = [
        'name',
        'description',
        'user_id',
        'entity_type',
        'entity_type_id',
        'entity_id',
    ];

    protected $perPage = 30;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class, 'entity_id', 'id');
    }

    public function image(): BelongsTo
    {
        return $this->belongsTo(Image::class, 'entity_id', 'id');
    }

    public function entity(): MorphTo
    {
        return $this->morphTo('entity');
    }
}
