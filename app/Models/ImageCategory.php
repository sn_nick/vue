<?php
declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ImageCategory extends Model
{
    protected $fillable = [
        'name'
    ];

    public $timestamps = false;

    public function images(): HasMany
    {
        return $this->hasMany(Image::class);
    }
}
